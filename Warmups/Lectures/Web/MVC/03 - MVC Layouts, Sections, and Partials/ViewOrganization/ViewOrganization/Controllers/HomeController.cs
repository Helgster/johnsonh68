﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewOrganization.Models;

namespace ViewOrganization.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Roster()
        {
            var players = new List<Player>
            {
                new Player {Number=84, Position="TE", Name="Jordan Cameron"},
                new Player {Number=12, Position="WR", Name="Josh Gordon"}
            };

            return View(players);
        }
	}
}