﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcModelBinding.Models
{
    public class BirthdayPersonBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var person = new BirthdayPerson();

            person.FirstName = controllerContext.HttpContext.Request.Form["FirstName"];
            person.LastName = controllerContext.HttpContext.Request.Form["LastName"];

            int month = int.Parse(controllerContext.HttpContext.Request.Form["month"]);
            int day = int.Parse(controllerContext.HttpContext.Request.Form["day"]);
            int year = int.Parse(controllerContext.HttpContext.Request.Form["year"]);

            person.BirthDay = new DateTime(year, month, day);

            return person;
        }
    }
}

