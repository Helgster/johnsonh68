﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQJoins
{
    class Program
    {
        static void Main()
        {
            List<Student> students = new List<Student>
                {
                    new Student {LastName = "Wise", StudentID = 1},
                    new Student {LastName = "Ward", StudentID = 2}
                };

            List<StudentCourse> courses = new List<StudentCourse>
                {
                    new StudentCourse {StudentID = 1, CourseName = "C# Fundamentals"},
                    new StudentCourse {StudentID = 1, CourseName = "ASP.NET Fundamentals"},
                    new StudentCourse {StudentID = 2, CourseName = "Java Fundamentals"},
                    new StudentCourse {StudentID = 2, CourseName = "CSS Fundamentals"},
                };

            var results = from student in students
                          join course in courses 
                          on student.StudentID equals course.StudentID
                          select new {course, student };
            
            Console.WriteLine("Student Courses");
            foreach (var r in results)
            {
                Console.WriteLine("{0,-10} {1}", r.student.LastName, r.course.CourseName);
            }

            Console.ReadLine();
        }
    }

    class Student
    {
        public int StudentID { get; set; }
        public string LastName { get; set; }
    }

    class StudentCourse
    {
        public int StudentID { get; set; }
        public string CourseName { get; set; }
    }
}
