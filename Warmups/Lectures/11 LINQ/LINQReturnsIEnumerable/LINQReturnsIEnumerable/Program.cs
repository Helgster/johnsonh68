﻿using System;
using System.Linq;

namespace LINQReturnsIEnumerable
{
    class Program
    {
        static void Main()
        {
            int[] ints = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};

            var lownumbers = from i in ints
                                          where i < 50
                                          select i;

            foreach(int i in lownumbers)
                Console.WriteLine(i);

            Console.ReadLine();
        }
    }
}
