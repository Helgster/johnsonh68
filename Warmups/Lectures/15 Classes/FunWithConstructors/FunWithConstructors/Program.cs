﻿using System;

namespace FunWithConstructors
{
    class Program
    {
        private static Sounder storedSounder;

        static void Main()
        {
            CreateSounders();
            storedSounder.SoundOff();

            var r = new Rectangle(10, 20);
            //var r2 = new Rectangle(10, -5);

            Console.ReadLine();
        }

        private static void CreateSounders()
        {
            var s1 = new Sounder();
            var s2 = new Sounder(10);
            var s3 = new Sounder(100, "Mary");

            s1.SoundOff();
            s2.SoundOff();
            s3.SoundOff();

            storedSounder = s1;
        }

        class Sounder
        {
            public int Id { get; set; }
            public string Name { get; set; }

            public Sounder()
            {
                Id = 28;
                Name = "Default";
            }

            public Sounder(int id)
            {
                Id = id;
                Name = "Default";
            }

            public Sounder(int id, string name)
            {
                Id = id;
                Name = name;
            }

            public void SoundOff()
            {
                Console.WriteLine("{0} : {1}", Id, Name);
            }
        }


    }

    class Rectangle
    {
        public int Width { get; set; }
        public int Length { get; set; }

        public Rectangle(int width, int length)
        {
            Width = width;
            Length = length;
        }
    }
}
