﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicInheritance
{
    class Program
    {
        static void Main( string[] args )
        {
            Console.WriteLine("***** Basic Inheritance *****\n");
            // Make a Car object and set max speed.
            Car myCar = new Car(80);

            // Set the current speed, and print it.
            myCar.Speed = 50;
            Console.WriteLine("My car is going {0} MPH", myCar.Speed);

            // Now make a MiniVan object.
            MiniVan myVan = new MiniVan();
            myVan.Speed = 10;
            Console.WriteLine("My van is going {0} MPH",
              myVan.Speed);


            List<Car> myCars = new List<Car>();
            for (int i = 40; i < 90; i+=10)
            {
                myCars.Add(new MiniVan(i));
            }

            foreach(Car c in myCars)
                Console.WriteLine("The car has a max speed of {0}", c.maxSpeed);

            Console.ReadLine();
        }
    }
}
