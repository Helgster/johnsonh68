﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicInheritance
{
    // MiniVan 'is-a' Car.
    class MiniVan : Car
    {
        public MiniVan() : base()
        {
            
        }

        public MiniVan(int maxSpeed) : base(maxSpeed)
        {
            
        }
    }
}
