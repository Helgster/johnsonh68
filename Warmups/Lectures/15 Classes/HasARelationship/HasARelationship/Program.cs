﻿using System;

namespace HasARelationship
{
    class Radio
    {
        public void TurnOn()
        {
            Console.WriteLine("Radio is turned on!");
        }
    }

    class Car
    {
        private Radio myRadio = new Radio();

        public void TurnOnRadio()
        {
            myRadio.TurnOn();
        }
    }

    class Program
    {
        static void Main()
        {
            var myCar = new Car();
            myCar.TurnOnRadio();
        }
    }
}
