﻿using System;

namespace FunWithStaticConstructors
{
    class Program
    {
        static void Main()
        {
            var p1 = new Player();
            p1.Name = "Joe";

            var p2 = new Player();
            p2.Name = "Jenn";

            while (p1.Score < 100 && p2.Score < 100)
            {
                p1.RollDice();
                p2.RollDice();
            }
            
            Console.WriteLine("Final Score: ");
            Console.WriteLine("{0} : {1}     {2} : {3}", p1.Name, p1.Score, p2.Name, p2.Score);
            Console.ReadLine();
        }
    }

    class Player
    {
        //private static Random RandomKey;

        //static Player()
        //{
        //    RandomKey = new Random();
        //}

        private static Random RandomKey;
        public Player()
        {
            RandomKey=new Random();
        }

        public string Name { get; set; }
        public int Score { get; set; }

        public void RollDice()
        {
            Score += RandomKey.Next(1,7) * 5;
        }
    }
}
