﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestableRockPaperScissors.Interfaces;

namespace TestableRockPaperScissors.Implementations
{
    public class SecondPlayerPicker : IChoiceSelector
    {
        public Choice GetComputerChoice()
        {
            Console.Clear();
            Console.Write("Player 2: Enter a choice (R)ock, (P)aper, (S)cissors: ");
            string input = Console.ReadLine();

            switch (input)
            {
                case "P":
                    return Choice.Paper;
                case "S":
                    return Choice.Scissors;
                default:
                    return Choice.Rock;
            }
        }
    }
}
