﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleInterfaces
{
    public class GermanShepherd : IDog, IBigDog
    {
        public void Bark()
        {
            Console.WriteLine("WOOF!");
        }

        public void Chase()
        {

        }
    }
}
