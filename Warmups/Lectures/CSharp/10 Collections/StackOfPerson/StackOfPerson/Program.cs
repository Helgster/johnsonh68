﻿using System;
using System.Collections.Generic;

namespace StackOfPerson
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack<Person> people = MakeStackOfPerson();

            Console.WriteLine("First person is: {0}", people.Peek().FirstName);
            Console.WriteLine("Popped {0} off the stack!\n", people.Pop().FirstName);
            
            Console.WriteLine("Second person is: {0}", people.Peek().FirstName);
            Console.WriteLine("Popped {0} off the stack!\n", people.Pop().FirstName);

            Console.WriteLine("Third person is: {0}", people.Peek().FirstName);
            Console.WriteLine("Popped {0} off the stack!\n", people.Pop().FirstName);

            try
            {
                // error, nothing left in the stack!
                Console.WriteLine("First person is: {0}", people.Peek().FirstName);
                Console.WriteLine("Popped {0} off the stack!\n", people.Pop().FirstName);
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine("\nError: {0}", ex.Message);                
            }

            Console.ReadLine();
        }

        private static Stack<Person> MakeStackOfPerson()
        {
            //stack is last in, first out
            Stack<Person> stack = new Stack<Person>();

            stack.Push(new Person { FirstName = "Homer", LastName = "Simpson", Age = 47 });
            stack.Push(new Person { FirstName = "Marge", LastName = "Simpson", Age = 45 });
            stack.Push(new Person { FirstName = "Lisa", LastName = "Simpson", Age = 9 });

            return stack;
        }
    }
}
