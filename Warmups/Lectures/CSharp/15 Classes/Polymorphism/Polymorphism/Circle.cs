﻿using System;

namespace Polymorphism
{
    public class Circle : Shape
    {
        public decimal Radius { get; set; }
        public override void Draw()
        {
            Console.WriteLine("Drawing a circle");
        }
    }
}
