﻿using System;

namespace Polymorphism
{
    class Program
    {
        static void Main()
        {
            Shape[] myShapes = new Shape[4];
            myShapes[0] = new Shape();
            myShapes[1] = new Circle() {Radius = 10};
            myShapes[2] = new Square();
            myShapes[3] = new Triangle();

            foreach (Shape s in myShapes)
            {
                if (s is Circle)
                {
                    Console.WriteLine("The radius is: {0} ", ((Circle)s).Radius);
                }
                s.Draw();
            }

            Console.ReadLine();
        }
    }
}
