﻿using System;

namespace FunWithObjectInitializers
{
    class Program
    {
        static void Main()
        {
            var p1 = new Point();
            p1.X = 5;
            p1.Y = 10;
            Console.WriteLine("Point({0},{1})", p1.X, p1.Y);

            var p2 = new Point {X = 5, Y = 10};
            Console.WriteLine("Point({0},{1})", p2.X, p2.Y);
        }
    }

    class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
