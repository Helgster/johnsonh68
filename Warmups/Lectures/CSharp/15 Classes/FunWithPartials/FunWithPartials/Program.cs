﻿namespace FunWithPartials
{
    class Program
    {
        static void Main()
        {
            var p = new Person();
            p.Name = "Joe Somebody";
            p.Phone = "330-444-1111";
            p.Email = "test@test.com";
        }
    }
}
