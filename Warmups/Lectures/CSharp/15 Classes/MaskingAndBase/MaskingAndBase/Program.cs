﻿using System;

namespace MaskingAndBase
{
    class Program
    {
        static void Main()
        {
            var d = new DerivedClass();
            d.PrintField1();

            Console.WriteLine();

            // we can also cast up to the base class
            Console.WriteLine(d.Field1);
            Console.WriteLine(((BaseClass)d).Field1);

            Console.ReadLine();
        }
    }

    class BaseClass
    {
        public string Field1 = "Base class Field1";
    }

    class DerivedClass : BaseClass
    {
        public new string Field1 = "Derived class Field1";

        public void PrintField1()
        {
            Console.WriteLine(Field1);
            Console.WriteLine(base.Field1);
        }
    }
}
