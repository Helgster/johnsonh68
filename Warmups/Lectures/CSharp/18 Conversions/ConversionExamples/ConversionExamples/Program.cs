﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversionExamples
{
    class Program
    {
        static void Main()
        {
            //SByteToShort();
            //ShortToSbyte();
            //CheckedBlock();
            //ReferenceTypeConversions();
            //Boxing();
            //Unboxing();
            ArrayListExample();
            //IsOperator();
            //AsOperator();

            Console.ReadLine();
        }


        private static void SByteToShort()
        {
            sbyte s1 = 5;
            short s2 = s1; // no problem, short is bigger

            Console.WriteLine(s2);
        }

        private static void ShortToSbyte()
        {
            short s1 = 5024;
            sbyte s2 = (sbyte)s1;

            Console.WriteLine("5024 as sbyte = {0}", s2);
        }

        private static void CheckedBlock()
        {
            short s1 = 5024;
            sbyte s2;

            try
            {
                checked
                {
                    s2 = (sbyte)s1;
                }

                Console.WriteLine("5024 as sbyte = {0}", s2);
            }
            catch (OverflowException)
            {
                Console.WriteLine("Can't convert {0} to sbyte", s1);
            }

        }

        private static void ReferenceTypeConversions()
        {
            Child myChild = new Child();
            Parent parent = myChild;
            IDisplayable displayable = myChild;

            Child anotherChild;
            //anotherChild = parent; // compile error, downcasting

            anotherChild = (Child) parent; // compiles, but isn't safe

            Console.WriteLine(anotherChild.ChildName);
        }

        private static void Boxing()
        {
            int i = 10;
            object obj = i;

            Console.WriteLine("i:{0} obj:{1}", i, obj);
        }

        private static void Unboxing()
        {
            int i = 10;
            object obj = i;

            int j = (int) i;


            Console.WriteLine("obj:{0} j:{1}", obj, j);
        }

        private static void ArrayListExample()
        {
            ArrayList arr = new ArrayList();
            List<int> myInts = new List<int>();

            for (int i = 1; i < 11; i++)
            {
                arr.Add(i);
                myInts.Add(i);
            }

            //arr.Add("Hello");

            foreach (object o in arr)
            {
                int i = (int)o;
                Console.WriteLine(i);
            }
        }

        private static void IsOperator()
        {
            IDisplayable child = new Child();

            if (child is Parent)
                Console.WriteLine("child is castable as Parent");

        }

        private static void AsOperator()
        {
            IDisplayable child = new Banana();

            Parent parent = child as Parent;
            if (parent == null)
                Console.WriteLine("child is NOT castable as Parent");
        }
    }
}
