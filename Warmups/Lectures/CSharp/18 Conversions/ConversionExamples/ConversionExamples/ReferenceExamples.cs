﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversionExamples
{
    public class Child : Parent, IDisplayable
    {
        public string ChildName { get; set; }

        public void Print(string message)
        {
            Console.WriteLine(message);
        }
    }

    public class Parent
    {
        public string ParentName { get; set; }
    }

    public interface IDisplayable
    {
        void Print(string message);
    }

    public class Banana : IDisplayable
    {
        public void Print(string message)
        {
            Console.WriteLine(message);
        }
    }


}
