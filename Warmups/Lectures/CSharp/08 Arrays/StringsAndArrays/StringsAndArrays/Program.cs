﻿using System;

namespace StringsAndArrays
{
    class Program
    {
        static void Main()
        {
            CharIndexingFunctionality();
            SplitFunctionality();
            Console.ReadLine();
        }

        private static void CharIndexingFunctionality()
        {
            string s1 = "This is a string of characters";

            foreach (char c in s1)
            {
                Console.WriteLine(c);
            }

            Console.WriteLine("The character at position 3 is {0}", s1[3]);
            Console.WriteLine("The length of s1 is {0}", s1.Length);
        }

        static void SplitFunctionality()
        {
            string[] words = "This is a sentence.".Split(' ');

            foreach (string s in words)
            {
                Console.WriteLine(s);
            }
        }
    }
}
