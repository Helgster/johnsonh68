﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutputParametersDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Point myPoint = new Point();

            GetPoint(myPoint);
            Console.WriteLine("x: {0}, y: {1}", myPoint.X, myPoint.Y);
            Console.ReadLine();
        }

        static void GetPoint(Point input)
        {
            input.X = 5;
            input.Y = 10;
        }
    }

    class Point
    {
        public int X;
        public int Y;
    }
}
