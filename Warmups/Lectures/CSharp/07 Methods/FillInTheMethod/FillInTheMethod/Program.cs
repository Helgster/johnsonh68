﻿using System;

namespace FillInTheMethod
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Is 3 greater than 5? {0}", IsNum1Greater(3,5));
            Console.WriteLine("Is 10 greater than 2? {0}", IsNum1Greater(10, 2));

            PrintMyAddress("George Costanza", "100 Broadway St", "New York", "NY", "10605");

            Console.WriteLine("10 + 10 = {0}", Add(10, 10));
            Console.WriteLine("10 - 2 = {0}", Subtract(10, 2));  // Create a subtract method
        }

        static int Add(int a, int b)
        {
            // return the sum
        }

        static bool IsNum1Greater(int num1, int num2)
        {
            // return true/false whether num1 is greater than num2
        }

        static void PrintMyAddress(string name, string street, string city, string state, string zipcode)
        {
            Console.WriteLine("Address");
            Console.WriteLine("------------------");

            // write out the address in a good format
        }
    }
}
