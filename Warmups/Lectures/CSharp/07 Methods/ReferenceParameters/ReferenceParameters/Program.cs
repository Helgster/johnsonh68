﻿using System;

namespace ReferenceParameters
{
    class MyClass { public int Val = 20; }
  
    class Program      
    {             
        static void ValueTypeAsValueParameter(MyClass f1, int f2)
        {
            f1.Val += 5;
            f2 += 5;
            Console.WriteLine("f1.Val: {0}, f2: {1}", f1.Val, f2);
        }

        static void ValueTypeAsRefParameter(ref MyClass f1, ref int f2)
        {
            f1.Val += 5;
            f2 += 5;
            Console.WriteLine("f1.Val: {0}, f2: {1}", f1.Val, f2);
        }

        static void ReferenceTypeAsValueParameterWithNew(MyClass f1)
        {
            // Assign to the object member.                   
            f1.Val = 50;
            Console.WriteLine("After member assignment: {0}", f1.Val);

            // Create a new object and assign it to the formal parameter.                   
            f1 = new MyClass();
            Console.WriteLine("After new object creation: {0}", f1.Val);
        }    

        static void ReferenceTypeAsRefParameterWithNew(ref MyClass f1)            
        {                  
            // Assign to the object member.                   
            f1.Val = 50;                   
            Console.WriteLine( "After member assignment: {0}", f1.Val );                  
            
            // Create a new object and assign it to the formal parameter.                   
            f1 = new MyClass();                   
            Console.WriteLine( "After new object creation: {0}", f1.Val );            
        }


        static void Main()
        {
            InvokeValueTypeAsValueParameter();
            Console.WriteLine("");

            InvokeValueTypeAsRefParameter();
            Console.WriteLine("");
            
            InvokeReferenceTypeAsValue();
            Console.WriteLine("");
            
            InvokeReferenceTypeAsRef();
            Console.WriteLine("");

            Console.ReadLine();
        }

        private static void InvokeValueTypeAsValueParameter()
        {
            MyClass a1 = new MyClass();
            int a2 = 10;

            Console.WriteLine("Value Type As Value");
            Console.WriteLine("-------------------");
            
            ValueTypeAsValueParameter(a1, a2);
            Console.WriteLine("a1.Val: {0}, a2: {1}", a1.Val, a2);
        }

        private static void InvokeValueTypeAsRefParameter()
        {
            MyClass a1 = new MyClass();
            int a2 = 10;

            Console.WriteLine("Value Type As Ref");
            Console.WriteLine("-------------------");

            ValueTypeAsRefParameter(ref a1, ref a2);
            Console.WriteLine("a1.Val: {0}, a2: {1}", a1.Val, a2);
        }

        private static void InvokeReferenceTypeAsValue()
        {
            MyClass a1 = new MyClass();

            Console.WriteLine("Method Without ref");
            Console.WriteLine("------------------");
            
            Console.WriteLine("Before method call: {0}", a1.Val);
            ReferenceTypeAsValueParameterWithNew(a1);
            Console.WriteLine("After method call: {0}", a1.Val);
        }

        private static void InvokeReferenceTypeAsRef()
        {
            MyClass a1 = new MyClass();

            Console.WriteLine("Method With ref");
            Console.WriteLine("---------------");

            Console.WriteLine("Before method call: {0}", a1.Val);
            ReferenceTypeAsRefParameterWithNew(ref a1);
            Console.WriteLine("After method call: {0}", a1.Val);
        }


    }
}
