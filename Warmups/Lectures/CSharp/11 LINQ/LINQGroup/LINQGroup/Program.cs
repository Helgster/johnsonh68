﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQGroup
{
    class Program
    {
        static void Main()
        {
            List<Student> students = new List<Student>
                {
                    new Student {LastName = "Sandler", Major = "Computer Science"},
                    new Student {LastName = "Martin", Major = "History"},
                    new Student {LastName = "Murphy", Major = "English"},
                    new Student {LastName = "Pacino", Major = "Computer Science"},
                    new Student {LastName = "Candy", Major = "History"},
                };

            var result = from student in students
                         group student by student.Major;

            foreach (var group in result)
            {
                Console.WriteLine(group.Key);

                foreach (var student in group)
                {
                    Console.WriteLine("\t{0}", student.LastName);
                }
            }

            Console.ReadLine();
        }
    }

    class Student
    {
        public string LastName { get; set; }
        public string Major { get; set; }
    }
}
