﻿using System;

namespace GuessingGame
{
    class Program
    {
        static void Main()
        {
            int theAnswer;
            int playerGuess;
            string playerInput;
            bool isNumberGuessed = false;

            Random r = new Random(); // This is System's built in random number generator
            theAnswer = r.Next(1, 21); // Get a random from 1 to 20

            do
            {
                // get player input
                Console.Write("Enter your guess: ");
                playerInput = Console.ReadLine();

                // attempt to convert to number
                if (int.TryParse(playerInput, out playerGuess))
                {
                    // see if they won
                    if (playerGuess == theAnswer)
                    {
                        Console.WriteLine("You got it!");
                        isNumberGuessed = true;
                    }
                    else
                    {
                        // they didn't win, so were they too high or too low?
                        if (playerGuess > theAnswer)
                            Console.WriteLine("Too high!");
                        else
                            Console.WriteLine("Too low!");
                    }
                }
                else
                {
                    // learn to play n00b
                    Console.WriteLine("That wasn't a valid number!");
                }
            } while (!isNumberGuessed);

            Console.WriteLine("Press any key to quit.");
            Console.ReadLine();
        }
    }
}
