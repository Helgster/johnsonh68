﻿using System;

namespace FunWithExceptions
{
    class Program
    {
        static void Main()
        {
            //CauseException();
            //HandleException();
            //HandleSpecificException();
            //DisplayException();
            //CallStackExample();
            //ThrowExample();
            ThrowCustomException();

            Console.ReadLine();
        }

        static void CauseException()
        {
            int x = 5;
            int y = 0;

            Console.WriteLine(x / y);
        }

        static void HandleException()
        {
            try
            {
                int x = 5;
                int y = 0;

                Console.WriteLine(x / y);
            }
            catch
            {
                Console.WriteLine("You did something bad, " +
                                  "but I'm going to keep running!");
            }
        }

        static void HandleSpecificException()
        {
            try
            {
                //throw new ArgumentNullException("blah");
                int[] ints = new int[2];
                ints[50] = 20;

                ints[0] = 5;
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Index out of range!");
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Divide by zero!");
            }
        }

        static void DisplayException()
        {
            try
            {
                Divide(5, 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine("The following error occurred");
                Console.WriteLine("----------------------------");
                Console.WriteLine(ex.Message);
                Console.WriteLine("Source: {0}", ex.Source);
                Console.WriteLine();

                Console.WriteLine("It happened here");
                Console.WriteLine("----------------");
                Console.WriteLine(ex.StackTrace);
            }
        }

        private static void ThrowCustomException()
        {
            try
            {
                throw new MySpecificException("boo");
            }
            catch (MySpecificException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }

        static void CallStackExample()
        {
            try
            {
                Method1();
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Catch clause in CallStackExample()");
            }
            finally
            {
                Console.WriteLine("Finally clause in call stack example");
            }

            Console.WriteLine("Keep running");
        }

        private static void Method1()
        {
            try
            {
                Method2();
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("Catch clause in Method 1");
            }
            finally
            {
                Console.WriteLine("Finally clause in Method 1");
            }
        }

        private static void Method2()
        {
            int x = 10, y = 0;
            try
            {
                Console.WriteLine(x/y);
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Catch clause in method 2");
            }
            finally
            {
                Console.WriteLine("Finally clause in method 2");
            }
        }

        static void ThrowExample()
        {
            try
            {
                PrintMessage("Hello!");
                PrintMessage("");
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void Divide(int x, int y)
        {
            Console.WriteLine(x / y);
        }

        private static void PrintMessage(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                throw new ArgumentNullException("message");
            }

            Console.WriteLine(message);
        }
    }
}
