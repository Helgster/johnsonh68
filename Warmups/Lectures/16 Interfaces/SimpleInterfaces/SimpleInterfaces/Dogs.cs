﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleInterfaces
{
    public interface IDog
    {
        void Bark();
    }

    public class Chihuahua : IDog
    {
        public void Bark()
        {
            Console.WriteLine("yip!");
        }
    }

    public class GermanShepherd : IDog
    {
        public void Bark()
        {
            Console.WriteLine("WOOF!");
        }
    }


}
