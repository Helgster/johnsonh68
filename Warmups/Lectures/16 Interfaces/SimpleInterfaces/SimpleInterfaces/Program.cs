﻿namespace SimpleInterfaces
{
    class Program
    {
        static void Main()
        {
            Chihuahua smallDog = new Chihuahua();
            GermanShepherd bigDog = new GermanShepherd();

            GiveTreat(smallDog);
            GiveTreat(bigDog);
        }

        static void GiveTreat(IDog dog)
        {
            dog.Bark();
        }
    }
}
