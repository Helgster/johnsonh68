﻿using System;

namespace IComparableExample
{
    class Program
    {
        static void Main()
        {
            var ints = new int[] {5, 3, 17, 4};
            Array.Sort(ints);

            foreach(var i in ints)
                Console.WriteLine(i);

            Console.ReadLine();

            // How about Temperatures?
            var temperatures = new Temperature[]
                            {
                                new Temperature {Fahrenheit = 32},
                                new Temperature {Fahrenheit = 212},
                                new Temperature {Fahrenheit = 100},
                                new Temperature {Fahrenheit = 50},
                            };

            Array.Sort(temperatures);
            foreach (var t in temperatures)
                Console.WriteLine(t.Fahrenheit);

            Console.ReadLine();
        }
    }
}
