﻿using System;
using System.Linq;

namespace NorthwindSample
{
    class Program
    {
        static void Main(string[] args)
        {
            //GetUSACustomers();
            RegionTest();
            Console.ReadLine();
        }

        private static void RegionTest()
        {
            Console.WriteLine("Creating region Test");

            using (var context = new NorthwindEntities())
            {
                Region r = new Region();
                r.RegionDescription = "Test";

                context.Regions.Add(r);
                context.SaveChanges();

                Console.WriteLine("New RegionID: {0}", r.RegionID);

                r.RegionDescription = "Test2";
                context.SaveChanges();

                Console.WriteLine("Check the db");
                Console.ReadLine();

                Console.WriteLine("Delete the new record");
                context.Regions.Remove(r);
                context.SaveChanges();

            }
        }

        private static void GetUSACustomers()
        {
            using (var context = new NorthwindEntities())
            {
                var usaCustomers = context.Customers.Where(c => c.Country == "USA");

                foreach (var customer in usaCustomers)
                {
                    Console.WriteLine("{0,-35} {1} {2}", customer.CompanyName, customer.Phone, customer.Country);
                }
            }
        }
    }
}
