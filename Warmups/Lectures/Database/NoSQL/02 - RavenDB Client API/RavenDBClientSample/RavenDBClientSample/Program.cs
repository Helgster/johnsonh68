﻿using Raven.Client.Document;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RavenDBClientSample
{
    class Program
    {
        static void Main(string[] args)
        {
            //CreateEmployee();
            //UpdateEmployee();
            //DeleteEmployee();
            //LoadEmployee();
            QueryExecutives();
            Console.ReadLine();
        }

        private static void QueryExecutives()
        {
            using (var ds = new DocumentStore
            {
                Url = "http://marvel:9000",
                DefaultDatabase = "Personel"
            }.Initialize())
            {
                // do stuff
                using (var session = ds.OpenSession())
                {
                    var executives = session.Query<Employee>().Where(e => e.Department == "Executive");

                    foreach (var e in executives)
                    {
                        Console.WriteLine("{0} {1} {2} {3} {4}", e.Id, e.FirstName, e.LastName, e.Department, e.ManagerId);
                    }
                    session.SaveChanges();
                }
            }
        }

        private static void DeleteEmployee()
        {
            using (var ds = new DocumentStore
            {
                Url = "http://marvel:9000",
                DefaultDatabase = "Personel"
            }.Initialize())
            {
                // do stuff
                using (var session = ds.OpenSession())
                {
                    var emp = session.Load<Employee>("Employees/1");

                    session.Delete(emp);
                    session.SaveChanges();
                }
            }
        }

        private static void UpdateEmployee()
        {
            using (var ds = new DocumentStore
            {
                Url = "http://marvel:9000",
                DefaultDatabase = "Personel"
            }.Initialize())
            {
                // do stuff
                using (var session = ds.OpenSession())
                {
                    var emp = session.Load<Employee>("Employees/1");

                    emp.LastName = "Landstrom";
                    session.SaveChanges();
                }
            }
        }

        private static void LoadEmployee()
        {
            using (var ds = new DocumentStore
            {
                Url = "http://marvel:9000",
                DefaultDatabase = "Personel"
            }.Initialize())
            {
                // do stuff
                using (var session = ds.OpenSession())
                {
                    var emp = session.Load<Employee>("Employees/1");

                    Console.WriteLine("{0} {1} {2} {3} {4}", emp.Id, emp.FirstName, emp.LastName, emp.Department, emp.ManagerId);
                }
            }
        }

        static void CreateEmployee()
        {
            using (var ds = new DocumentStore
            {
                Url = "http://marvel:9000",
                DefaultDatabase = "Personel"
            }.Initialize())
            {
                // do stuff
                using (var session = ds.OpenSession())
                {
                    var emp = new Employee
                    {
                        Id = "Employees/1",
                        Department = "IT",
                        FirstName = "Eric",
                        LastName = "Wise",
                        ManagerId = "Employees/2"
                    };

                    session.Store(emp);

                    var emp2 = new Employee
                    {
                        Id = "Employees/2",
                        Department = "Executive",
                        FirstName = "Lisette",
                        LastName = "Landstrom"
                    };

                    session.Store(emp2);

                    session.SaveChanges();
                }
            }
        }
    }
}
