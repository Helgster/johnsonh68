use master
go

create database MyWebStore;

use MyWebStore
go

create table [dbo].[Cart](
	[CartID] [int] IDENTITY(1, 1) not null primary key,
	[StartTime] [DateTime] not null,
    [Session] uniqueidentifier not null
)

create table [dbo].[CartItems](
	[CartItemID] [int] IDENTITY(1, 1) not null primary key,
	[CartID] [int] not null,
	[Quantity] [int] not null,
	[ProductID] [int] not null,
	[UnitPrice] [decimal(9,2)] not null,
	[Total] [decimal(9,2)] not null
	)

create table [dbo].[Products] (
	[ProductID] [int] IDENTITY(1, 1) not null primary key,
	[Name] [varchar](50) not null,
	[UnitPrice] [decimal(9,2)] not null,
	[CategoryID] [int] null
	)

create table [dbo].[Category](
	[CategoryID] [int] IDENTITY(1, 1) not null primary key,
	[CategoryName] [varchar](50) not null
	)

create table [dbo].[Payment](
	[PaymentID] [int] IDENTITY(1, 1) not null primary key,
	[Type] [varchar](20) not null,
	[Amount] [decimal(9,2)] not null
	)

create table [dbo].[Invoice](
	[InvoiceID] [int] IDENTITY(1, 1) not null primary key,
	[CustomerName] [varchar](50) not null,
	[Street1] [varchar](50) not null,
	[Street2] [varchar](50) null,
	[City] [varchar](50) not null,
	[State] [char](2) not null,
	[Zip] [varchar](10) not null,
	[Total] [decimal(9,2)] not null,
	[PaymentID] [int] null
)

create table [dbo].[InvoiceItem](
	[InvoiceItemID] [int] IDENTITY(1, 1) not null primary key,
	[InvoiceID] [int] not null,
	[ProductID] [int] not null,
	[Quantity] [int] not null,
	[UnitPrice] [decimal(9,2)] not null,
	[Total] [decimal(9,2)] not null
	)

alter table [dbo].[CartItems]
	add constraint FK_CartItems_Cart foreign key (CartID)
	references [dbo].[Cart] (CartID)

alter table [dbo].[CartItems]
	add constraint FK_CartItems_Products foreign key (ProductID)
	references [dbo].[Products] (ProductID)

alter table [dbo].[Products]
	add constraint FK_Products_Category foreign key (CategoryID)
	references [dbo].[Category](CategoryID)

alter table [dbo].[Invoice]
	add constraint FK_Invoice_Payment foreign key (PaymentID)
	references [dbo].[Payment](PaymentID)

alter table [dbo].[InvoiceItem]
	add constraint FK_InvoiceItem_Invoice foreign key (InvoiceID)
	references [dbo].[Invoice](InvoiceID)

alter table [dbo].[InvoiceItem]
	add constraint FK_InvoiceItem_Product foreign key (ProductID)
	references [dbo].[Products](ProductID)