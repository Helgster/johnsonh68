SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.RegionInsert
	@RegionDescription nchar(50)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO dbo.Region
           (RegionDescription)
    VALUES
           (@RegionDescription);

	SELECT SCOPE_IDENTITY() AS RegionId;
END
GO

CREATE PROCEDURE dbo.RegionUpdate
	@RegionID int,
	@RegionDescription nchar(50)
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE dbo.Region
		SET RegionDescription = @RegionDescription
    WHERE
		RegionID = @RegionID;
END
GO

CREATE PROCEDURE dbo.RegionDelete
	@RegionID int
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM dbo.Region
    WHERE
		RegionID = @RegionID;
END
GO