﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionExamples
{
    class Program
    {
        static void Main(string[] args)
        {
            ShowArrayList();
            ShowHashTable();
            ShowStack();
            ShowQueue();
        }

        private static void ShowArrayList()
        {
            ArrayList intList = new ArrayList();
            // when we add something to the arraylist, it is "upcasted" to object type (boxed)
            intList.Add(1);
            intList.Add(5);
            intList.Add(10);
            intList.Add(4);

            int sum = 0;
            // since everything is an object, we need to cast it back to an int (unboxed)
            foreach (object o in intList)
            {
                sum += (int) o;
            }

            Console.WriteLine("The sum of the arraylist is: {0}", sum);
            Console.ReadLine();
            Console.Clear();
        }

        private static void ShowHashTable()
        {
            // A HashTable is a key/value pair.  
            //Keys must be unique and values can be accessed by key
            Hashtable applicationMap = new Hashtable();

            applicationMap.Add("txt", "notepad.exe");
            applicationMap.Add("bmp", "paint.exe");
            applicationMap.Add("jpg", "paint.exe");
            applicationMap.Add("docx", "word.exe");

            Console.Write("Enter an extension: ");
            string extension = Console.ReadLine();

            Console.WriteLine("We would open extension {0} with {1}", 
                extension, applicationMap[extension]);

            // add a new key to the hashtable
            Console.Write("Enter a new extension: ");
            extension = Console.ReadLine();

            Console.Write("Enter a new app: ");
            string application = Console.ReadLine();

            if (applicationMap.ContainsKey(extension))
            {
                Console.WriteLine("That key already exists!");
            }
            else
            {
                applicationMap.Add(extension, application);
            }

            // print all values
            Console.WriteLine("\nPrint all values\n_________________");
            foreach (var key in applicationMap.Keys)
            {
                Console.WriteLine(applicationMap[key]);
            }

            Console.ReadLine();
            Console.Clear();
        }

        private static void ShowStack()
        {
            Stack myStack = new Stack();
            myStack.Push("Hello");
            myStack.Push("World");
            myStack.Push("!");

            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine(myStack.Pop());
            }

            Console.ReadLine();
            Console.Clear();
        }

        private static void ShowQueue()
        {
            Queue myQueue = new Queue();
            myQueue.Enqueue("Hello");
            myQueue.Enqueue("World");
            myQueue.Enqueue("!");

            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine(myQueue.Dequeue());
            }

            Console.ReadLine();
            Console.Clear();
        }
    }
}
