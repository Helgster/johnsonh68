﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictionaryOfPerson
{
    class Program
    {
        static void Main(string[] args)
        {
            Person p1 = new Person() { FirstName = "Homer", LastName = "Simpson", ID = 1 };
            Person p2 = new Person() { FirstName = "Marge", LastName = "Simpson", ID = 2 };
            Person p3 = new Person() { FirstName = "Lisa", LastName = "Simpson", ID = 3 };
            Person p4 = new Person() {FirstName = "Bart", LastName = "Simpson", ID = 4};

            Dictionary<int,Person> dictionary = new Dictionary<int, Person>();

            dictionary.Add(p1.ID, p1);
            dictionary.Add(p2.ID, p2);
            dictionary.Add(p3.ID, p3);
            dictionary.Add(p4.ID, p4);

            foreach (int key in dictionary.Keys)
            {
                PrintPerson(dictionary[key]);
            }

            Console.ReadLine();
        }

        static void PrintPerson(Person p)
        {
            Console.WriteLine("{0} {1},{2}", p.ID, p.LastName, p.FirstName);
        }
    }
}
