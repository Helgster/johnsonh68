﻿using System;
using System.Collections.Generic;

namespace QueueOfPerson
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<Person> people = MakeQueueOfPerson();

            Console.WriteLine("First person is: {0}", people.Peek().FirstName);
            Console.WriteLine("Popped {0} off the stack!\n", people.Dequeue().FirstName);

            Console.WriteLine("First person is: {0}", people.Peek().FirstName);
            Console.WriteLine("Popped {0} off the stack!\n", people.Dequeue().FirstName);

            Console.WriteLine("First person is: {0}", people.Peek().FirstName);
            Console.WriteLine("Popped {0} off the stack!\n", people.Dequeue().FirstName);

            try
            {
                // error, nothing left in the stack!
                Console.WriteLine("First person is: {0}", people.Peek().FirstName);
                Console.WriteLine("Popped {0} off the stack!\n", people.Dequeue().FirstName);
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine("\nError: {0}", ex.Message);
            }

            Console.ReadLine();
        }

        private static Queue<Person> MakeQueueOfPerson()
        {
            //stack is last in, first out
            Queue<Person> stack = new Queue<Person>();

            stack.Enqueue(new Person { FirstName = "Homer", LastName = "Simpson", Age = 47 });
            stack.Enqueue(new Person { FirstName = "Marge", LastName = "Simpson", Age = 45 });
            stack.Enqueue(new Person { FirstName = "Lisa", LastName = "Simpson", Age = 9 });

            return stack;
        }
    }
}
