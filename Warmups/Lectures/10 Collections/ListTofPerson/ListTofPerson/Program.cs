﻿using System;
using System.Collections.Generic;

namespace ListTofPerson
{
    class Program
    {
        static void Main(string[] args)
        {
            //List<Person> people = LongVersionOfInitializing();
            //List<Person> people2 = ShorthandVersionOfInitializing();

            //Console.WriteLine("Long");
            //DisplayPeople(people);

            //Console.WriteLine("\nShort");
            //DisplayPeople(people2);

            OtherListMembers();
            Console.ReadLine();
        }

        private static void OtherListMembers()
        {
            List<int> numbers = new List<int>();

            numbers.Add(1);
            numbers.AddRange(new int[] {5,6,7,8});

            numbers.Insert(2, 100);
            //numbers.RemoveRange(2,3);
            //numbers.Remove(6);
            //numbers.RemoveAt(0);

            Console.WriteLine(numbers[2]);

            foreach (var num in numbers)
            {
                Console.WriteLine(num);
            }

            for (int i = 0; i < numbers.Count; i++)
            {
                Console.WriteLine(numbers[i]);
            }

        }

        private static void DisplayPeople(List<Person> people)
        {

            Console.WriteLine("Total people: {0}", people.Count);

            foreach (Person person in people)
            {       
                Console.WriteLine("{0}, {1,-10} Age: {2:D2}", person.LastName, person.FirstName, person.Age);
            }
        }

        private static List<Person> LongVersionOfInitializing()
        {
            var people = new List<Person>();

            Person p1 = new Person();
            p1.FirstName = "Homer";
            p1.LastName = "Simpson";
            p1.Age = 47;

            people.Add(p1);

            Person p2 = new Person();
            p2.FirstName = "Marge";
            p2.LastName = "Simpson";
            p2.Age = 45;

            people.Add(p2);

            Person p3 = new Person();
            p3.FirstName = "Lisa";
            p3.LastName = "Simpson";
            p3.Age = 9;

            people.Add(p3);

            Person p4 = new Person();
            p4.FirstName = "Bart";
            p4.LastName = "Simpson";
            p4.Age = 8;

            people.Add(p4);

            return people;
        }

        /// <summary>
        /// We can use object initialization syntax in collections too
        /// </summary>
        /// <returns></returns>
        private static List<Person> ShorthandVersionOfInitializing()
        {
            return new List<Person>
                       {
                           new Person {FirstName = "Homer", LastName = "Simpson", Age = 47},
                           new Person {FirstName = "Marge", LastName = "Simpson", Age = 45},
                           new Person {FirstName = "Lisa", LastName = "Simpson", Age = 9},
                           new Person {FirstName = "Bart", LastName = "Simpson", Age = 8},
                       };
        }
    }
}
