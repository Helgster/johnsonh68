﻿using System;

namespace HelloWorldSample
{
    class Program // define our class
    {
        static void Main() // start point of the application
        {
            Console.WriteLine("Hi there!");
        }
    }
}
