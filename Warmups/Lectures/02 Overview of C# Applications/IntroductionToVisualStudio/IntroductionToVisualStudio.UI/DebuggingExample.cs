﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroductionToVisualStudio.UI
{
    public class DebuggingExample
    {
        public string CaughtException()
        {
            string fileData = "";
            try
            {
                fileData = File.ReadAllText("does_not_exist.txt");
            }
            catch
            {
                // never swallow exceptions!
            }
            

            return fileData;
        }

        public string UnhandledException()
        {
            string fileData = "";

            fileData = File.ReadAllText("does_not_exist.txt");

            return fileData;
        }

        public void BreakPoint()
        {
            int[] myNumbers = new int[10];

            for (int i = 0; i < 10; i++)
            {
                myNumbers[i] = i + 1;
            }

        }

        public int Add(int x, int y)
        {
            return x + y;
        }
    }
}
