﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DateTimeDemo;


namespace TestDateTimes
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            int wednesdays = Program.GetWednesdays(DateTime.Today, DateTime.Today.AddMonths(1));
            Assert.AreEqual(4, wednesdays);
        }
    }
}
