﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DateTimeDemo
{
    public class Program
    {
        static void Main(string[] args)
        {
            DateTime inputtedDate;
            string input;
            do
            {
                Console.WriteLine("Please enter a date:");
                input = Console.ReadLine();
            } while (!DateTime.TryParse(input, out inputtedDate));

            int numWednesdays = GetWednesdays(DateTime.Today, inputtedDate);

            Console.WriteLine("There are {0} Wednesdays between today and {1}", numWednesdays, inputtedDate.ToString("d"));
            Console.ReadLine();
        }

        public static int GetWednesdays(DateTime startDate, DateTime endDate)
        {
            int numWednesdays = 0;

            //do
            //{
            //    if (startTime.AddDays(7) <= endTime){
            //        numWednesdays++;
            //    }
            //    startTime = startTime.AddDays(7);
            //} while (startTime < endTime);
            for (DateTime dt = DateTime.Today; dt <= endTime; dt = dt.AddDays(7) )
            {
                if (startTime.AddDays(7) <= endTime)                
                {
                    numWednesdays++;
                }
                startDate = startDate.AddDays(7);
            } while (startDate <= endDate);


            return numWednesdays;
        }
    }
}
