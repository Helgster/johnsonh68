﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProductInventoryDemo
{
    public class Barcode : IComparable
    {
        public string Upc { get; set; }

        // my recommendation is to convert the underlying
        // UPC to a long (since it's really just a big number)
        // and return the result of comparing one long to another
        public int CompareTo(object obj)
        {
            if (obj is Barcode)
            {
                Barcode objAsBarcode = obj as Barcode;
                long thisBarcode = long.Parse(this.Upc);
                long objBarcode = long.Parse(objAsBarcode.Upc);

                if (thisBarcode < objBarcode)
                {
                    return -1;
                }
                else if (thisBarcode == objBarcode)
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
            else
            {
                throw new ArgumentException("You can't compare a barcode and not a barcode!!!");
            }
        }
    }
}
