﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductInventoryDemo
{
    public class BlueJean : Product
    {
        public override string Color
        {
            get
            {
                return "Blue";
            }
        }

        public int WaistSize { get; set; }
        public int Length { get; set; }
    }
}
