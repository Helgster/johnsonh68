﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductInventoryDemo
{
    static class Program
    {
        static void Main(string[] args)
        {
            List<Barcode> barcodes = new List<Barcode>()
            {
                new Barcode() {Upc = "5"},
                new Barcode() {Upc = "7"},
                new Barcode() {Upc = "9"},
                new Barcode() {Upc = "100"},
                new Barcode() {Upc = "2"},
                new Barcode() {Upc = "8"},
            };

            barcodes.Sort();

            foreach (Barcode b in barcodes)
            {
                Console.WriteLine(b.Upc);
            }


            Inventory inv = new Inventory();
            inv.Products.Add(new Product());
            string input = "";

            do
            {
                Console.WriteLine("What kind of product do you want to add?");
                Console.WriteLine("Press C for coat, J for blue jean, S for shirt.");
                Console.WriteLine("Press q to stop adding products");
                input = Console.ReadLine();
                Product prod = GetProductDetails(input);
                if (prod != null)
                {
                    inv.Products.Add(prod);
                }
            } while (input != "q");

            // this gets the number of shirts in my inventory....
            int quantityOfShirts = inv.Products.Where(x => x is Shirt).Count();

        }

        static Product GetProductDetails(string productType)
        {
            Product retProd = new Product();

            if (productType == "C")
            {
                retProd = new Coat();
                retProd = FillInProductDetails(retProd);

                Console.WriteLine("Enter in whether or not the coat is fur");
                var torfStr = Console.ReadLine();
                ((Coat)retProd).IsFur = bool.Parse(torfStr);

                Console.WriteLine("Enter in the length");
                var lenStr = Console.ReadLine();
                ((Coat)retProd).Length = int.Parse(lenStr);

                return retProd;
            }
            else if (productType == "J")
            {
                // add jeans code here
                return retProd;
            }
            else if (productType == "S")
            {
                //add shirt code here;
                return retProd;
            }
            else
            {
                return null;
            }
        }

        static Product FillInProductDetails(Product prod)
        {
            Console.WriteLine("Please enter the product price");
            string decimalValue = Console.ReadLine();
            prod.Price = Decimal.Parse(decimalValue);

            // fill in the rest of the product properties
            Console.WriteLine("Please give me the barcode");
            string barcode = Console.ReadLine();
            Barcode barcodeObj = new Barcode();
            barcodeObj.Upc = barcode;
            prod.Sku = barcodeObj;

            return prod;
        }
    }
}
