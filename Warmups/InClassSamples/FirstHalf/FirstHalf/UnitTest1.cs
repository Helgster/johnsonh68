﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FirstHalf
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            string testStr = "WooHoo";
            FirstHalfSolution firstHalfInstance = new FirstHalfSolution();
            string result = firstHalfInstance.FirstHalf(testStr);
            Assert.AreEqual("Woo", result);
        }

        [TestMethod]
        public void TestMethod2()
        {
            string testStr = "HelloThere";
            FirstHalfSolution firstHalfInstance = new FirstHalfSolution();
            string result = firstHalfInstance.FirstHalf(testStr);
            Assert.AreEqual("Hello", result);
        }

        [TestMethod]
        public void TestMethod3()
        {
            string testStr = "abcdef";
            FirstHalfSolution firstHalfInstance = new FirstHalfSolution();
            string result = firstHalfInstance.FirstHalf(testStr);
            Assert.AreEqual("abc", result);
        }
    }
}
