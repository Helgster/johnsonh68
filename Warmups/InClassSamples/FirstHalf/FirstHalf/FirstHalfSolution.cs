﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstHalf
{
    class FirstHalfSolution
    {
        public string FirstHalf(string str)
        {
            string returnStr = "";
            if (str.Length % 2 == 0)
            {
                returnStr = str.Substring(0, str.Length / 2);
            }
            return returnStr;
        }
    }
}
