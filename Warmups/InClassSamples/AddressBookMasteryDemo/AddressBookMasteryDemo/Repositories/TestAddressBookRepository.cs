﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AddressBookMasteryDemo.Models;

namespace AddressBookMasteryDemo.Repositories
{
    public class TestAddressBookRepository : IAddressBookRepository
    {
        public AddressBook Load()
        {
            return new AddressBook()
            {
                Entries = new List<Entry>()
                {
                    new Entry(){
                        FirstName = "Joe",
                        LastName = "Schmoe",
                        PhoneNumber = "1800CARPETS",
                        Email = "joe@carpets.com",
                        Address = "123 Fake st. Minneapolis, MN 55401"
                    },
                    new Entry(){
                        FirstName = "Sheryl",
                        LastName = "Schmoe",
                        PhoneNumber = "1800CARPETS",
                        Email = "sheryl@carpets.com",
                        Address = "123 Fake st. Minneapolis, MN 55401"
                    }
                }
            };
        }

        public void Save(AddressBook addyBook)
        {
            return;
        }
    }
}
