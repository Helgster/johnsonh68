﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AddressBookMasteryDemo.Models;
using System.IO;

namespace AddressBookMasteryDemo.Repositories
{
    public class FileAddressBookRepository : IAddressBookRepository
    {
        private const string FileName = @"Data\AddressBook.txt";

        public AddressBook Load()
        {
            AddressBook addyBook = new AddressBook();
            using (StreamReader reader = new StreamReader(FileName))
            {
                string aLine = reader.ReadLine();
                while (aLine != null)
                {
                    Entry newEntry = new Entry();

                    string[] fields = aLine.Split(',');

                    newEntry.FirstName = fields[0];
                    newEntry.LastName = fields[1];
                    newEntry.Address = fields[2];
                    newEntry.PhoneNumber = fields[3];
                    newEntry.Email = fields[4];

                    addyBook.Entries.Add(newEntry);

                    aLine = reader.ReadLine();
                }
            }

            return addyBook;
        }

        public void Save(AddressBook addyBook)
        {
            using (StreamWriter writer = new StreamWriter(FileName))
            {
                foreach (Entry entry in addyBook.Entries)
                {
                    string[] fields = new string[5];
                    
                    fields[0] = entry.FirstName;
                    fields[1] = entry.LastName;
                    fields[2] = entry.Address;
                    fields[3] = entry.PhoneNumber;
                    fields[4] = entry.Email;

                    string fieldsStr = string.Join(",", fields);

                    writer.WriteLine(fieldsStr);
                }
            }
        }
    }
}
