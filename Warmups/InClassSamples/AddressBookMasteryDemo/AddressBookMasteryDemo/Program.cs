﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AddressBookMasteryDemo.Repositories;
using AddressBookMasteryDemo.Models;
using AddressBookMasteryDemo.Utilities;

namespace AddressBookMasteryDemo
{
    class Program
    {
        private static IAddressBookRepository _addressBookRepo = new FileAddressBookRepository();
        private static AddressBook _addyBook = new AddressBook();

        static void Main(string[] args)
        {
            GenerateAddressBookUtils.GenerateAddressBookFile();
            string userSelectedOption = "";
            do
            {
                userSelectedOption = GetUserSelectedOption();
                switch (userSelectedOption)
                {
                    case "1":
                        _addyBook = _addressBookRepo.Load();
                        break;
                    case "2":
                        _addressBookRepo.Save(_addyBook);
                        break;
                    case "3":
                        AddEntry();
                        break;
                    case "4":
                        RemoveEntry();
                        break;
                    case "5":
                        EditEntry();
                        break;
                    case "6":
                        SortEntries();
                        break;
                    case "7":
                        SearchForEntry();
                        break;
                    case "8":
                        break;
                    default:
                        // seriously?
                        Console.WriteLine("That's not a valid option, please select again.");
                        Console.WriteLine("");
                        break;
                }
                Console.WriteLine("");
            } while (userSelectedOption != "8");
        }

        static void SortEntries()
        {
            _addyBook.SortEntries();
            Console.WriteLine("");
            foreach (var entry in _addyBook.Entries)
            {
                Console.WriteLine("{0}, {1}, {2}, {3}, {4}", entry.FirstName, entry.LastName, entry.Address, entry.PhoneNumber, entry.Email);
            }
            Console.WriteLine("");
        }

        static void EditEntry()
        {
            Console.Write("Enter the exact last name you'd like edited in the address book: ");
            var lastName = Console.ReadLine();
            Entry entry = _addyBook.FindEntryToEdit(lastName);

            Console.Write("What is the first name: ");
            entry.FirstName = Console.ReadLine();
            Console.Write("What is the last name: ");
            entry.LastName = Console.ReadLine();
            Console.Write("What is the address: ");
            entry.Address = Console.ReadLine();
            Console.Write("What is the phone number: ");
            entry.PhoneNumber = Console.ReadLine();
            Console.Write("What is the email address: ");
            entry.Email = Console.ReadLine();

        }

        static void RemoveEntry()
        {
            Console.Write("Enter the exact last name you'd like removed from the address book: ");
            var lastName = Console.ReadLine();
            int totalEntriesDeleted = _addyBook.RemoveEntryByLastName(lastName);

            Console.WriteLine();
            Console.WriteLine("Deleted {0} entries", totalEntriesDeleted);
            Console.WriteLine();

        }

        static void AddEntry()
        {
            Console.Write("What is the first name: ");
            var fName = Console.ReadLine();
            Console.Write("What is the last name: ");
            var lName = Console.ReadLine();
            Console.Write("What is the address: ");
            var address = Console.ReadLine();
            Console.Write("What is the phone number: ");
            var phoneNumber = Console.ReadLine();
            Console.Write("What is the email address: ");
            var email = Console.ReadLine();

            _addyBook.Entries.Add(new Entry()
            {
                FirstName = fName,
                LastName = lName,
                PhoneNumber = phoneNumber,
                Address = address,
                Email = email 
            });
        }

        static void SearchForEntry()
        {
            Console.Write("Please enter a search term to search by last name: ");
            var searchTerm = Console.ReadLine();

            var matches = _addyBook.SearchForEntryByLastName(searchTerm);

            Console.WriteLine("");
            Console.WriteLine("We found {0} matches", matches.Count());
            foreach (var match in matches)
            {

                Console.WriteLine("");
                Console.WriteLine("First Name: {0}", match.FirstName);
                Console.WriteLine("Last Name: {0}", match.LastName);
                Console.WriteLine("Address: {0}", match.Address);
                Console.WriteLine("Phone Number: {0}", match.PhoneNumber);
                Console.WriteLine("Email: {0}", match.Email);
            }
        }

        static string GetUserSelectedOption()
        {
            Console.WriteLine("1) Load from file");
            Console.WriteLine("2) Save to file");
            Console.WriteLine("3) Add an entry");
            Console.WriteLine("4) Remove an entry");
            Console.WriteLine("5) Edit an existing entry");
            Console.WriteLine("6) Sort the address book");
            Console.WriteLine("7) Search for a specific entry");
            Console.WriteLine("8) Quit");
            Console.WriteLine("");
            Console.Write("Please choose what you'd like to do with the database: ");
            string selectedOption = Console.ReadLine();

            return selectedOption;
        }
    }
}
