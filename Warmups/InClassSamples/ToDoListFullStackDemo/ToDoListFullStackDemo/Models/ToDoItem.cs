﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoListFullStackDemo.Models
{
    public class ToDoItem
    {
        public int Id { get; set; }
        public string ToDo { get; set; }
        public DateTime DueDate { get; set; }
        public bool IsCompleted { get; set; }
    }
}