﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ToDoListFullStackDemo.BizLogic;
using ToDoListFullStackDemo.Models;

namespace ToDoListFullStackDemo.Controllers.api
{
    public class ToDoApiController : ApiController
    {
        private BusinessLogic _bll = new BusinessLogic();

        public void Post(List<ToDoItem> items)
        {
            _bll.SaveAllToDoItems(items);
        }
    }
}
