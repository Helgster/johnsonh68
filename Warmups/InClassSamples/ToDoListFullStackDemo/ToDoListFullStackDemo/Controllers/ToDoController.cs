﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDoListFullStackDemo.BizLogic;
using ToDoListFullStackDemo.Models;

namespace ToDoListFullStackDemo.Controllers
{
    public class ToDoController : Controller
    {
        private BusinessLogic _bll = new BusinessLogic();
        // GET: ToDo
        public ActionResult Index()
        {
            return View(_bll.GetAllToDoItems());
        }
    }
}