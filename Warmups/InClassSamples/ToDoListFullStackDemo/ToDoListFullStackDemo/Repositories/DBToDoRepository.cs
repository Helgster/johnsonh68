﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;

namespace ToDoListFullStackDemo.Repositories
{
    public class DBToDoRepository : IToDoRepository
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["ToDoDatabase"].ConnectionString;

        public void Add(Models.ToDoItem item)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                var commandText = "INSERT INTO ToDo (ToDo, DueDate, IsCompleted) VALUES (@ToDo, @DueDate, @IsCompleted)";
                SqlCommand command = new SqlCommand(commandText, conn);
                command.Parameters.AddWithValue("@ToDo", item.ToDo);
                command.Parameters.AddWithValue("@DueDate", item.DueDate);
                command.Parameters.AddWithValue("@IsCompleted", item.IsCompleted);

                command.Connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public void Delete(int id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                var commandText = "DELETE FROM ToDo WHERE Id = @Id";
                SqlCommand command = new SqlCommand(commandText, conn);
                command.Parameters.AddWithValue("@Id", id);

                command.Connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public void Edit(Models.ToDoItem item)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                var commandText = "UPDATE ToDo SET ToDo = @ToDo, DueDate = @DueDate, IsCompleted = @IsCompleted WHERE Id = @Id";
                SqlCommand command = new SqlCommand(commandText, conn);
                command.Parameters.AddWithValue("@Id", item.Id);
                command.Parameters.AddWithValue("@ToDo", item.ToDo);
                command.Parameters.AddWithValue("@DueDate", item.DueDate);
                command.Parameters.AddWithValue("@IsCompleted", item.IsCompleted);

                command.Connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public Models.ToDoItem GetToDoItemById(int id)
        {
            return GetToDoItems().FirstOrDefault(x => x.Id == id);
        }

        public List<Models.ToDoItem> GetToDoItems()
        {
            List<Models.ToDoItem> todoItems = new List<Models.ToDoItem>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("SELECT * FROM ToDo", conn);
                command.Connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Models.ToDoItem newItem = new Models.ToDoItem();
                    newItem.Id = reader.GetInt32(0);
                    newItem.ToDo = reader.GetString(1);
                    newItem.DueDate = reader.GetDateTime(2);
                    newItem.IsCompleted = reader.GetBoolean(3);
                    todoItems.Add(newItem);
                }
            }

            return todoItems;
        }


        public void SaveAllToDoItems(List<Models.ToDoItem> items)
        {
            this.GetToDoItems().ForEach(x => this.Delete(x.Id));

            items.ForEach(x => this.Add(x));
        }
    }
}