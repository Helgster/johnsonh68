﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoListFullStackDemo.Models;

namespace ToDoListFullStackDemo.Repositories
{
    public interface IToDoRepository
    {
        void Add(ToDoItem item);
        void Delete(int id);
        void Edit(ToDoItem item);
        void SaveAllToDoItems(List<ToDoItem> items);
        ToDoItem GetToDoItemById(int id);
        List<ToDoItem> GetToDoItems();
    }
}
