﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToDoListFullStackDemo.Models;
using ToDoListFullStackDemo.Repositories;

namespace ToDoListFullStackDemo.BizLogic
{
    public class BusinessLogic
    {
        private IToDoRepository _repo = new DBToDoRepository();

        public void AddToDoItem(ToDoItem item)
        {
            _repo.Add(item);
        }

        public void EditToDoItem(ToDoItem item)
        {
            _repo.Edit(item);
        }

        public void DeleteToDoItem(ToDoItem item)
        {
            _repo.Delete(item.Id);
        }

        public ToDoItem GetToDoItem(int id)
        {
            return _repo.GetToDoItemById(id);
        }

        public List<ToDoItem> GetAllToDoItems()
        {
            return _repo.GetToDoItems();
        }

        public void MarkCompleted(ToDoItem item)
        {
            item.IsCompleted = true;
            _repo.Edit(item);
        }

        public void SaveAllToDoItems(List<ToDoItem> items)
        {
            _repo.SaveAllToDoItems(items);
        }

    }
}