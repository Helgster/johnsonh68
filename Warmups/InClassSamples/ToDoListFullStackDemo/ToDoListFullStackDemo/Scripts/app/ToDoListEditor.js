﻿function regenerateALLTheThings() {
    $('.datepicker').datepicker({ minDate: 0 });
    $('.delete-btn').click(function () {
        $(this).parents('.todo-item').remove();
    });
}

$(function () {
    regenerateALLTheThings();
    $('.add-btn').click(function () {
        var htmlString = '<div class="row todo-item">';
        htmlString += '<div class="col-md-4">';
        var id = Math.floor((Math.random() * Number.MAX_VALUE) + 1);
        htmlString += '<input type="text" id="todo' + id + '" name="ToDo" />';
        htmlString += '</div>';
        htmlString += '<div class="col-md-2"><input type="text" id="duedate' + id + '" class="datepicker" name="DueDate" /></div>';
        htmlString += '<div class="col-md-1"><input type="checkbox" id="completed' + id + '" name="IsCompleted" /></div>';
        htmlString += '<div class="col-md-1"><button class="btn btn-danger delete-btn" id="delBtn' + id + '">X</button></div>';
        htmlString += '</div>';
        $('.table-of-items').append(htmlString);
        regenerateALLTheThings();
    });

    $('#save-btn').click(function () {
        var arrayOfValues = [];
        
        $.each($('.todo-item'), function (i, todoitem) {
            var todo = {
                Id: $(todoitem).find('input[name="Id"]').val(),
                ToDo: $(todoitem).find('input[name="ToDo"]').val(),
                DueDate: $(todoitem).find('input[name="DueDate"]').val(),
                IsCompleted: $(todoitem).find('input[name="IsCompleted"]').is(':checked')
            };
            arrayOfValues.push(todo);
        });

        $.ajax
            ({
                type: 'POST',
                url: '/api/ToDoApi',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(arrayOfValues),
                success: function () {
                    alert('saved');
                }
            });

    });
});