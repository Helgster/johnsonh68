USE master

CREATE LOGIN ToDoApp WITH PASSWORD = 'Aw3s0m3P@ssw0rd!!!1'

CREATE DATABASE ToDoDB

USE ToDoDB

CREATE USER ToDoApp FOR LOGIN ToDoApp

CREATE TABLE ToDo 
(
	Id INT IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	ToDo NVARCHAR(MAX),
	DueDate DATETIME,
	IsCompleted BIT
)

EXEC sp_addrolemember 'db_datareader', 'ToDoApp'
EXEC sp_addrolemember 'db_datawriter', 'ToDoApp'

 --INSERT INTO ToDo (ToDo, DueDate, IsCompleted) VALUES ('Make cookies', '2020-01-01 00:00:00', 0)
  --INSERT INTO ToDo (ToDo, DueDate, IsCompleted) VALUES ('Do solo X-Country Flight', '2015-07-01 00:00:00', 0)