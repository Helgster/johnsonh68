﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class PhoneNumber
    {
        private string _theNumber;
        private string _typeOfNumber;

        public void SetNumber(string number)
        {
            _theNumber = number;
        }

        public void SetType(string type)
        {
            _typeOfNumber = type;
        }
    }
}
