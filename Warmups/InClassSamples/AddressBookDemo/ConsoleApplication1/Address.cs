﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Address
    {
        public enum StateList
        {
            MN,
            WI,
            IL,
            IA,
            SD,
            ND
        }

        private string _name;
        private string _streetAddress;
        private string _cityName;
        private StateList _state;
        private string _zip;
        private PhoneNumber[] _phoneNumbers = new PhoneNumber[1024];
        private string _emailAddress;
        private DateTime _birthday;

        public void Set_State(StateList theState)
        {
            _state = theState;
        }

        public StateList Get_State()
        {
            return _state;
        }


        public void AddPhoneNumber(string number, string type)
        {
            for (int i = 0; i < _phoneNumbers.Length; i++ )
            {
                if (_phoneNumbers[i] == null)
                {
                    _phoneNumbers[i] = new PhoneNumber();
                    _phoneNumbers[i].SetNumber(number);
                    _phoneNumbers[i].SetType(type);
                    break;
                }
            }
        }

        public void Set_Name(string theName)
        {
            _name = theName;
        }

        public string Get_Name()
        {
            return _name;
        }

        public void Set_Address(string theAddress)
        {
            _streetAddress = theAddress;
        }

        public string Get_Address()
        {
            return _streetAddress;
        }
    }
}
