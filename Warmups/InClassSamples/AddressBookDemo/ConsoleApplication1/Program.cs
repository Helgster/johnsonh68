﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Address addy1 = new Address();
            addy1.AddPhoneNumber("1-800-CARPETS", "Work");
            addy1.AddPhoneNumber("1-800-ASKGARY", "Home");
            addy1.Set_Name("Alec Wojciechowski");
            addy1.Set_State(Address.StateList.WI);
            string actualStateName = Enum.GetName(typeof(Address.StateList), addy1.Get_State());
            Console.WriteLine(addy1.Get_Name());
            Console.WriteLine(addy1.Get_State());
            Console.ReadLine();
        }
    }
}
