﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SwcLms.Models;

namespace SwcLms.Repositories
{
    public class DbUserRepository : IUserRepository
    {

        public void CreateLmsUser(Models.LmsUser user)
        {
            using (var context = new SWC_LMSEntities())
            {
                context.LmsUsers.Add(user);

                context.SaveChanges();
            }
        }


        public List<LmsUser> GetUnauthorizedUsers()
        {
            using (var context = new SWC_LMSEntities())
            {
                return context.LmsUsers.ToList();
            }
        }
    }
}