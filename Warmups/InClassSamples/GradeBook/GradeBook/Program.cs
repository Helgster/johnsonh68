﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeBook
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> assignments = new List<string>();
            string assignmentName = "";
            do
            {
                // let's get assignment names...we're not getting students scores just quite yet....
                Console.WriteLine("Enter in an assignment name. Enter q to stop entering assignments");
                assignmentName = Console.ReadLine();
                if (assignmentName != "q")
                {
                    assignments.Add(assignmentName);
                }
            } while (assignmentName != "q");

            // A list of type student
            List<Student> studentList = new List<Student>();
            string studentName = "";
            string studentAddress = "";
            string studentPhoneNumber = "";
            string studentNotes = "";
            do {
                // get user input for the student
                Console.WriteLine("Enter in an student name. Enter q to stop entering students");
                studentName = Console.ReadLine();
                Console.WriteLine("Enter in {0}'s address", studentName);
                studentAddress = Console.ReadLine();
                Console.WriteLine("Enter in {0}'s phone number", studentName);
                studentPhoneNumber = Console.ReadLine();
                Console.WriteLine("Enter in {0}'s special notes", studentName);
                studentNotes = Console.ReadLine();

                if (studentName != "q"){
                    // if the student name isn't q, let's add a new Student to the List!
                    Student newStudent = new Student();
                    newStudent.Name = studentName;
                    newStudent.Address = studentAddress;
                    newStudent.PhoneNumber = studentPhoneNumber;
                    newStudent.SpecialNotes = studentNotes;

                    // Add() expects a Student object since it's a list of type Student.
                    studentList.Add(newStudent);
                }
            } while(studentName != "q");

            foreach (string assignment in assignments)
            {
                foreach (Student student in studentList)
                {
                    string theScore = "";
                    int numScore = -1;
                    do
                    {
                        Console.WriteLine("Enter a score for {0} for assignment {1}", student.Name, assignment);
                        theScore = Console.ReadLine();
                        bool couldParse = int.TryParse(theScore, out numScore);
                        // we have to do this since tryparse assigns numScore to 0 if it can't parse theScore
                        if (!couldParse)
                        {
                            numScore = -1;
                        }
                    } while (numScore < 0 || numScore > 100);

                    // Let's add an assigment (which is the name of the assignment and the score)
                    // to the current student's List of Assignments.
                    Assignment anAssignment = new Assignment();
                    anAssignment.Name = assignment;
                    // thanks for document that the Score property must be between 0 and 100!!!
                    anAssignment.Score = numScore;
                    student.Assignments.Add(anAssignment);
                }
            }
        }
    }
}
