﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18_LastChars
{
    class Program
    {
    //Given 2 strings, a and b, return a new string made of the first char of a and the last char of b, so "yo" and "java" yields "ya".
    //If either string is length 0, use '@' for its missing char. 

    //LastChars("last", "chars") -> "ls"
    //LastChars("yo", "mama") -> "ya"
    //LastChars("hi", "") -> "h@"

    //public string LastChars(string str) {

    //}
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter your first word");
            var firWrd = Console.ReadLine();
            Console.WriteLine("Please enter your second word");
            var secWrd = Console.ReadLine();
            var lc = LastChars(firWrd, secWrd);
            Console.WriteLine(lc);
            Console.ReadLine();
        }
        public static string LastChars(string aS, string bS)
        {
            if (aS.Length >= 1 && bS.Length >= 1)
                return aS.Substring(0, 1) + bS.Substring(bS.Length - 1, 1);
            else if (aS.Length >= 1 && bS.Length == 0)
                return aS.Substring(0, 1) + "@";
            else if (aS.Length == 0 && bS.Length >= 1)
                return "@" + bS.Substring(bS.Length - 1, 1);
            else
                return "@@";
        }
    }
}
