﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _24_sTripX
{
    class Program
    {
    //Given a string, if the first or last chars are 'x', return the string without those 'x' chars, and otherwise return the string unchanged. 

    //StripX("xHix") -> "Hi"
    //StripX("xHi") -> "Hi"
    //StripX("Hxix") -> "Hxi"

    //public string StripX(string str) {

    //}
        
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a word  with an x in front, middle, or back,  or any combination of the following");
            var w1 = Console.ReadLine();
            var xx = StripX(w1);
            Console.WriteLine(xx);
            Console.ReadLine();
        }
        public static string StripX(string str)
        {
            var ss1 = str.Substring(0, 1);
            var ss2 = str.Substring(str.Length - 1, 1);
            var ss3 = str.Substring(1, str.Length - 2);

            if (ss1 == "x" && ss2 == "x")
                return ss3;
            else if (ss1 == "x")
                return ss3 + ss2;
            else if (ss2 == "x")
                return ss1 + ss3;

            return str;
        }
    }
}
