﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20_SwapLast
{
    class Program
    {
    //Given a string of any length, return a new string where the last 2 chars, if present, are swapped, so "coding" yields "codign". 

    //SwapLast("coding") -> "codign"
    //SwapLast("cat") -> "cta"
    //SwapLast("ab") -> "ba"

    //public string SwapLast(string str) {

    //}
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a word");
            var myWord = Console.ReadLine();
            var swplst = SwapLast(myWord);
            Console.WriteLine(swplst);
            Console.ReadLine();
        }
        public static string SwapLast(string str)
        {

            if (str.Length >= 2)
                return str.Substring(0, str.Length - 2) + str.Substring(str.Length - 1) + str.Substring(str.Length - 2, 1);
            else
                return str;
        }
    }
}
