﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace makeTags
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a tag");
            var tag = Console.ReadLine();
            Console.WriteLine("Please enter your content name");
            var content = Console.ReadLine();
            var output = MakeTags(tag, content);
            Console.WriteLine(output);
            Console.ReadLine();
        }
        public static string MakeTags(string tag, string content)
        {
            var pattern = "<" + tag + ">" + content + "<" + tag + "/>";
            return pattern;
        }
    }
}
