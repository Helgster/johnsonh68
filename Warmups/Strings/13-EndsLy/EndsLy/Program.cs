﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EndsLy
{
    class Program
    {
        static void Main(string[] args)
        {
        //Given a string, return true if it ends in "ly". 

        //EndsWithLy("oddly") -> true
        //EndsWithLy("y") -> false
        //EndsWithLy("oddy") -> false

            Console.WriteLine("Please enter a word I will tell you if it ends in ly");
            string myWord = Console.ReadLine();
            var ends = EndsWithLy(myWord);
            Console.WriteLine("Does your word end in ly?  {0}", ends);
            Console.ReadLine();

        }

        public static bool EndsWithLy(string str)
        {
            if (str.EndsWith("ly"))
            {
                return true;
            }
            else
            {
                return false;
            }


        }
    }
}
