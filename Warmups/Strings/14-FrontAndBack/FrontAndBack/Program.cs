﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontAndBack
{
    class Program
    {
        static void Main(string[] args)
        {
        //Given a string and an int n, return a string made of the first and last n chars from the string. The string length will be at least n. 

        //FrontAndBack("Hello", 2) -> "Helo"
        //FrontAndBack("Chocolate", 3) -> "Choate"
        //FrontAndBack("Chocolate", 1) -> "Ce"
            Console.WriteLine("Enter a word:");
            var myWord = Console.ReadLine();
            Console.WriteLine("Enter a number:");
            var myNum = Console.ReadLine();
            int num = Int32.Parse(myNum);
            var FaB = FrontAndBack(myWord, num);
            Console.WriteLine(FaB);
            Console.ReadLine();


        }
        public static string FrontAndBack(string str, int n)
        {
            var s1 = str.Substring(str.Length - n, n);
            var s2 = str.Substring(0, n);
            return s2 + s1;

        }
    }
}
