﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiddleTwo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a word");
            var myWord = Console.ReadLine();
            var MT = MiddleTwo(myWord);
            Console.WriteLine(MT);
            Console.ReadLine();
        }
        public static string MiddleTwo(string str)
        {
           int half = str.Length / 2 - 1;
	       return str.Substring(half, half);
        }
    }
}

