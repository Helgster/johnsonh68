﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abba
{
    class Program
    {
        static void Main(string[] args)
        {
            string a = "Me";
            string b = "Mine";
            var c1 = Abba(a, b);
            Console.WriteLine(c1);
            Console.ReadLine();
        }

        public static string Abba(string a, string b)
        {
            var myAbba = string.Concat(a, b, b, a);
            return myAbba;
        }
    }
}
