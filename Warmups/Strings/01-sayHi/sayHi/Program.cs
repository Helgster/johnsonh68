﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sayHi
{
    class Program
    {
        //Given a string name, e.g. "Bob", return a greeting of the form "Hello Bob!". 

        //SayHi("Bob") -> "Hello Bob!"
        //SayHi("Alice") -> "Hello Alice!"
        //SayHi("X") -> "Hello X!"

        //public string SayHi(string name) {

        //}
        
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter your name:");
            var input = Console.ReadLine();
            var greet = SayHi(input);
            Console.WriteLine(greet);
            Console.ReadLine();
        }
        public static string SayHi(string name)
        {
            var greeting = "Hello, " + name + "!";
            return greeting;
        }
    }
}
