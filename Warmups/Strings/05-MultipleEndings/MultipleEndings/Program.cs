﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultipleEndings
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a string:");
            var myStr = Console.ReadLine();
            var multiEnd = MultipleEndings(myStr);
            Console.WriteLine(multiEnd);
            Console.ReadLine();
        }
        public static string MultipleEndings(string myStr)
        {
           var str1 =  myStr.Substring(myStr.Length - 2, 2);
            return str1+str1+str1;
        }
    }
}
