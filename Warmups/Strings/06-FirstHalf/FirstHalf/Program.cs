﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstHalf
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a multiple word string:");
            var myStr = Console.ReadLine();
            var fH = FirstHalf(myStr);
            Console.WriteLine(fH);
            Console.ReadLine();
       }
        public static string FirstHalf(string myStr)
        {
            var str1 = myStr.Substring(0, myStr.Length / 2);
            return str1;
        }
    }
}
