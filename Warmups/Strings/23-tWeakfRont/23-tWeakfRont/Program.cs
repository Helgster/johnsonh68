﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23_tWeakfRont
{
    class Program
    {
    //Given a string, return a version without the first 2 chars.
    //Except keep the first char if it is 'a' and keep the second char if it is 'b'. 
    //The string may be any length.

    //TweakFront("Hello") -> "llo"
    //TweakFront("away") -> "aay"
    //TweakFront("abed") -> "abed"

    //public string TweakFront(string str) {

    //}
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a word");
            var s1 = Console.ReadLine();
            var twkfrt = TweakFront(s1);
            Console.WriteLine(twkfrt);
            Console.ReadLine();
        }
        public static string TweakFront(string str)
        {
            var ss1 = str.Substring(0, 1);
            var ss2 = str.Substring(1, 1);
            var ss3 = str.Substring(2, str.Length - 2);

            if (str.Length >= 2)
            {
                if (ss1 == "a" && ss2 == "b")
                    return ss1 + ss2 + ss3;
                else if (ss1 == "a")
                    return ss1 + ss3;
                else if (ss2 == "b")
                    return ss2 + ss3;
                else
                    return ss3;
            }
            return "";
        }
    }
}
