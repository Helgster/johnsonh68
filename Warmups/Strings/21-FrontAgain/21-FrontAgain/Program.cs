﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _21_FrontAgain
{
    class Program
    {
    //Given a string, return true if the first 2 chars in the string also appear at the end of the string, such as with "edited". 

    //FrontAgain("edited") -> true
    //FrontAgain("edit") -> false
    //FrontAgain("ed") -> true

    //public bool FrontAgain(string str) {

    //}
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a word");
            var myWord = Console.ReadLine();
            var frtag = FrontAgain(myWord);
            Console.WriteLine(frtag);
            Console.ReadLine();
        }
        public static bool FrontAgain(string str)
        {
            if (str.Length >= 2 && str.Substring(0, 2) == (str.Substring(str.Length - 2, 2)))
                return true;

            else
                return false;
        }
    }
}
