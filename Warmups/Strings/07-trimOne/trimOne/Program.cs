﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trimOne
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a word");
            var myWord = Console.ReadLine();
            var trimW = TrimOne(myWord);
            Console.WriteLine(trimW);
            Console.ReadLine();
        }
        public static string TrimOne(string myWord)
        {
            var str1 = myWord.Substring(1, myWord.Length - 2);
            return str1;
        }
    }
}
