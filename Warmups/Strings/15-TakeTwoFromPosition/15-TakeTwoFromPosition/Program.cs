﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _15_TakeTwoFromPosition
{
    class Program
    {
    //Given a string and an index, return a string length 2 starting at the given index.
    //If the index is too big or too small to define a string length 2, use the first 2 chars.
    //The string length will be at least 2. 

    //TakeTwoFromPosition("java", 0) -> "ja"
    //TakeTwoFromPosition("java", 2) -> "va"
    //TakeTwoFromPosition("java", 3) -> "ja"

    //public string TakeTwoFromPosition(string str, int n) {

    //}
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a word:");
            var myWord = Console.ReadLine();
            Console.WriteLine("Enter a number:");
            var myNum = Console.ReadLine();
            int num = Int32.Parse(myNum);
            var tTfP = TakeTwoFromPosition( myWord, num);
            Console.WriteLine(tTfP);
            Console.ReadLine();
        }
        public static string TakeTwoFromPosition(string str, int n)
        {
            if (n <= str.Length - 2 && n >= 0)
                return str.Substring(n, n + 2);
            return str.Substring(0, 2);
        }
    }
}
