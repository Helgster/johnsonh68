﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19_ConCat
{
    class Program
    {
    //Given two strings, append them together (known as "concatenation") and return the result.
    //However, if the concatenation creates a double-char, then omit one of the chars, so "abc" and "cat" yields "abcat". 

    //ConCat("abc", "cat") -> "abcat"
    //ConCat("dog", "cat") -> "dogcat"
    //ConCat("abc", "") -> "abc"

    //public string ConCat(string a, string b) {

    //}
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a string");
            var s1 = Console.ReadLine();
            Console.WriteLine("Please enter another string");
            var s2 = Console.ReadLine();
            var cCat = ConCat(s1, s2);
            Console.WriteLine(cCat);
            Console.ReadLine();
        }
        public static string ConCat(string a, string b)
        {
            
            if (a.Length >= 1 && b.Length >= 1)
            {
                if (a.Substring(a.Length - 1, 1) == b.Substring(0, 1))
                    return (a + b.Substring(1));
                else
                    return (a + b);
            }
            return (a + b);
        }
    }
}