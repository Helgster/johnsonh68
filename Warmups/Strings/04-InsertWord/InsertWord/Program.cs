﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsertWord
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What characters do you want to surround with");
            var surrounders = Console.ReadLine();
            Console.WriteLine("What word do you want to surround?");
            var theWord = Console.ReadLine();
            var output = InsertWord(surrounders, theWord);
            Console.WriteLine(output);
            Console.ReadLine();
        }
        public static string InsertWord(string container, string word)
        {
            var returnStr = container.Substring(0, 2) + word + container.Substring(2, 2);
            return returnStr;
        }
    }
}
