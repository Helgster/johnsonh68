﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rotateRight2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a word");
            var myWord = Console.ReadLine();
            var roR2 = RotateRight2(myWord);
            Console.WriteLine(roR2);
            Console.ReadLine();
        }

        public static string RotateRight2(string myWord)
        {
            var str1 = myWord.Substring(0, myWord.Length -2);
            var str2 = myWord.Substring(myWord.Length - 2, 2);
            return str2 + str1;
        }
    }
}
