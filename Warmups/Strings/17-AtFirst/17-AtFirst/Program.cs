﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _17_AtFirst
{
    class Program
    {
    //Given a string, return a string length 2 made of its first 2 chars. If the string length is less than 2, use '@' for the missing chars. 

    //AtFirst("hello") -> "he"
    //AtFirst("hi") -> "hi"
    //AtFirst("h") -> "h@"

    //public string AtFirst(string str) {

    //}
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a word");
            var myWord = Console.ReadLine();
            var atFir = AtFirst(myWord);
            Console.WriteLine(atFir);
            Console.ReadLine();
        }
        public static string AtFirst(string str)
        {
            if (str.Length >= 2)
                return str.Substring(0, 2);
            else if (str.Length == 1)
                return str.Substring(0, 1) + "@";
            else
                return "@@";
        }
    }
}
