﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _22_minCat
{
    class Program
    {
    //Given two strings, append them together (known as "concatenation") and return the result. However, if the strings are different lengths, omit chars from the longer string so it is the same length as the shorter string. So "Hello" and "Hi" yield "loHi". The strings may be any length. 

    //MinCat("Hello", "Hi") -> "loHi"
    //MinCat("Hello", "java") -> "ellojava"
    //MinCat("java", "Hello") -> "javaello"

    //public string MinCat(string a, string b) {

    //}
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a word");
            var s1 = Console.ReadLine();
            Console.WriteLine("Please enter another word");
            var s2 = Console.ReadLine();
            var mycat = MinCat(s1, s2);
            Console.WriteLine(mycat);
            Console.ReadLine();
        }
        public static string MinCat(string a, string b)
        {
            if (a.Length >= b.Length)
                return (a.Substring(a.Length - b.Length) + b);
            else
                return (a + b.Substring(b.Length  - a.Length));
        }
    }
}
