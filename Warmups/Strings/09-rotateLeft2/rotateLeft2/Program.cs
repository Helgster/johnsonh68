﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rotateLeft2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a word");
            var myWord = Console.ReadLine();
            var roLef = Rotateleft2(myWord);
            Console.WriteLine(roLef);
            Console.ReadLine();

        }
        public static string Rotateleft2(string myWord)
        {
            var str1 = myWord.Substring(0, 2);
            var str2 = myWord.Substring(2);
            return str2 + str1;
        }
    }
}
