﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeOneTF
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a word");
            var myWord = Console.ReadLine();
            Console.WriteLine("Enter (true or false)");
            var value = Console.ReadLine();
            var torf = Convert.ToBoolean(value);
            var roR2 = TakeOne(myWord, torf);
            Console.WriteLine(roR2);
            Console.ReadLine();
        }
        public static string TakeOne(string str, bool FromFront)
        {
            if (FromFront)
            {
                return str.Substring(0, 1);
            }
            return str.Substring(str.Length - 1);
            }
    }
}

