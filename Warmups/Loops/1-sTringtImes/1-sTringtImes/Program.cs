﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_sTringtImes
{
    class Program
    {
    //Given a string and a non-negative int n, return a larger string that is n copies of the original string. 

    //StringTimes("Hi", 2) -> "HiHi"
    //StringTimes("Hi", 3) -> "HiHiHi"
    //StringTimes("Hi", 1) -> "Hi"

    //public string StringTimes(string str, int n) {

    //}
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a word");
            var w1 = Console.ReadLine();
            Console.WriteLine("Please enter a number");
            var in1 = Console.ReadLine();
            int n1 = Int32.Parse(in1);
            var r1 = StringTimes(w1, n1);
            Console.WriteLine(r1);
            Console.ReadLine();
        }
        public static string StringTimes(string str, int n)
        {
            return (String.Concat(Enumerable.Repeat(str, n)));
        }
    }
}
