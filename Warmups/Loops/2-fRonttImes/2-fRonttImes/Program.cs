﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_fRonttImes
{
    class Program
    {
    //Given a string and a non-negative int n, we'll say that the front of the string is the first 3 chars, or whatever is there if the string is less than length 3. Return n copies of the front; 

    //FrontTimes("Chocolate", 2) -> "ChoCho"
    //FrontTimes("Chocolate", 3) -> "ChoChoCho"
    //FrontTimes("Abc", 3) -> "AbcAbcAbc"

    //public string FrontTimes(string str, int n) {

    //}
        static void Main(string[] args)
        {
            Console.Write("Please enter a word: ");
            var w1 = Console.ReadLine();
            Console.Write("Please enter a number: ");
            var n1 = Console.ReadLine();
            var cn1 = int.Parse(n1);
            var fTn = FrontTimes(w1, cn1);
            Console.WriteLine();
            Console.WriteLine(fTn);
            Console.ReadLine();
        }
        public static string FrontTimes(string str, int n)
        {
            var nstr = str.Substring(0, 3);
            return (String.Concat(Enumerable.Repeat(nstr, n)));
        }
    }
}
