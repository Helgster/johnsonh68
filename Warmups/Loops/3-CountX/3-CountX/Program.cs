﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_CountX
{
    class Program
    {
    //Count the number of "xx" in the given string. We'll say that overlapping is allowed, so "xxx" contains 2 "xx". 

    //CountXX("abcxx") -> 1
    //CountXX("xxx") -> 2
    //CountXX("xxxx") -> 3

    //public int CountXX(string str) {

    //}
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a word");
            var word = Console.ReadLine();
            var fTn = CountXX(word);
            Console.WriteLine();
            Console.WriteLine(fTn);
            Console.ReadLine();

        }
        public static int CountXX(string str)
        {
            int count = 0;
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (str.Substring(i + 2).Equals("xx"))
                {
                    count++;
                }
            }
            return count;
        }
    }
}
