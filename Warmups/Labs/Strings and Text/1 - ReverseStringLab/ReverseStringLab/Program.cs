﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReverseStringLab
{
    class Program
    {
        //Reverse a String
        //Difficulty: 1

        //Create a program that asks the user for a string of input and simply returns it in reverse order. 
        //For instance they enter “Hello” and it returns “olleH”.

        //Tips
        //-------------------------------
        //Keep in mind that a string is often treated as an array of individual characters.
        //One of the fastest ways to reverse a string is simply to swap each character as you work your way inward. 
        //So for instance, you would swap “H” with “o” and then “e” with “l” etc.
        //To do this, use a loop that continues until the start meets or surpasses the end.
        
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a sentence and I will print it back in reverse");
            var mySent = Console.ReadLine();
            var restring = ReverseString(mySent);
            Console.WriteLine(restring);
            Console.ReadLine();
        }
        public static string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }
    }

}
