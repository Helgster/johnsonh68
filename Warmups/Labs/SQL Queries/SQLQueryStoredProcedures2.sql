--1. SWCCorp needs a stored procedure called GetProductListByCategory
--   that should take in the category name as a parameter

CREATE PROCEDURE GetProductListByCategory
(
  @Name varchar(10)
 
) AS

SELECT *
FROM CurrentProducts
WHERE ProductName = @Name

GO