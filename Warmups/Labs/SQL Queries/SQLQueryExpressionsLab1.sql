--1. In Northwind, print a list of products, the value of the stock (unit price * quantity) and sort it by the value from most to least
SELECT UnitsInStock * UnitPrice AS InStockValue
FROM [Products]
ORDER BY InStockValue DESC

--2.In Northwind, get a list of employees with a column called NameLastFirst which should be formatted as 
--  LastName, FirstName.  Sort it alpha by last name then first name
SELECT LastName + ', ' + FirstName AS NameLastFirst
FROM Employees
ORDER BY LastName, FirstName

--3. Take your query from #1 and also create columns to value the stock in 
--   Canadian Dollars, Japanese Yen, Euros, and Pesos given today�s exchange rates
SELECT UnitsInStock * UnitPrice AS InStockValue,
       ROUND((UnitsInStock * (UnitPrice * 1.23)), 2) AS CanadianValue,
	   UnitsInStock * (UnitPrice * 123.61) AS JapaneseYenValue,
	   UnitsInStock * (UnitPrice * 0.89) AS EuroValue,
	   UnitsInStock * (UnitPrice * 15.49) AS PesosValue
FROM [Products]
ORDER BY InStockValue DESC


