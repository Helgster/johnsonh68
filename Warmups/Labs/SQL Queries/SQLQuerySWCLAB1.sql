SELECT *
FROM Employee
CROSS JOIN Location
ORDER BY LastName

SELECT *
FROM Employee
LEFT OUTER JOIN [Grant]
ON Employee.EmpID = [Grant].EmpID
WHERE [Grant].EmpID IS NULL
