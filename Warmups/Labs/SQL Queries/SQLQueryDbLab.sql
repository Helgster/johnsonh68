Create Database DbLab
go

Use DbLab
go

create table Customer
(CustomerID int Primary Key,
CustomerType varchar(30) NOT NULL,
FirstName varchar(20) NULL,
LastName varchar(30) NULL,
CompanyName varchar(30) NULL)
GO

INSERT INTO Customer VALUES (1,'Consumer','Mark','Williams',NULL)
INSERT INTO Customer VALUES (2,'Consumer','Lee','Young',NULL)
INSERT INTO Customer VALUES (3,'Consumer','Patricia','Martin',NULL)
INSERT INTO Customer VALUES (4,'Consumer','Mary','Lopez',NULL)
INSERT INTO Customer VALUES (5,'Business',NULL,NULL,'MoreTechnology.com')
