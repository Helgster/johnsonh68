--1. Re-run the movie catalogue script to put the data back the way it was

SELECT *
FROM Movie

--2. Add a new integer field ReleaseYear to the movie table

ALTER TABLE Movie
ADD [Release Year] Int NULL

--3. Add the Teaser column back into the movie table with a varchar(200) not null

ALTER TABLE Movie
ADD [Teaser] varchar(200) NOT NULL
DEFAULT 'Teaser Coming Soon'


--4. Rename the Runtime column to RuntimeMinutes

EXEC sp_rename 'Movie.Runtime', 'Movie.RuntimeMinutes'
