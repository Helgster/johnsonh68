--1. Find the Max, Min, and Average Grant amount by Employee ID

SELECT EmpID, MAX(Amount) AS MaxGrant
FROM [Grant]
WHERE EmpID IS NOT NULL
GROUP BY EmpID

SELECT EmpID, MIN(Amount) AS MinGrant
FROM [Grant]
WHERE EmpID IS NOT NULL
GROUP BY EmpID

SELECT EmpID, AVG(Amount) AS AvgGrant
FROM [Grant]
WHERE EmpID IS NOT NULL
GROUP BY EmpID

SELECT e.EmpID, FirstName, LastName, MAX(Amount) AS MaxGrant
FROM [Grant] g
INNER JOIN Employee e ON g.EmpID = e.EmpID
GROUP BY e.EmpID, FirstName, LastName
ORDER BY MaxGrant DESC