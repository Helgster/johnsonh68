--1. Find a list of customers with more than 10 orders

SELECT c.CompanyName, c.CustomerID, COUNT(o.OrderID) AS TotalOrders
FROM Orders o
INNER JOIN Customers c ON o.CustomerID = c.CustomerID
GROUP BY c.CompanyName, c.CustomerID
HAVING COUNT(o.OrderID) > 10
ORDER BY TotalOrders DESC

--2. Find Employees that have more than 100 orders.  Only the name and count should be outputted, it should look like this:
--   EmployeeName            OrderCount
--   LastName, FirstName     ####

SELECT (e.LastName + ', ' + e.FirstName) AS EmployeeName,
		COUNT(o.OrderID) AS OrderCount
FROM Employees e
INNER JOIN Orders o ON e.EmployeeID = o.EmployeeID
GROUP BY e.LastName, e.FirstName
HAVING COUNT(o.OrderID) > 100
ORDER BY OrderCount DESC
	
	 
