--1. Sally (employeeID 11) is getting married, change her last name to Green

SELECT *
FROM Employee
WHERE EmpID = 11

UPDATE Employee
SET LastName = 'Green'
WHERE EmpID = 11

--2. All the employees in the Spokane location are becoming contractors, update their status field to External

SELECT *
FROM Employee e
INNER JOIN Location l ON e.LocationID = l.LocationID
WHERE l.City = 'Spokane'

UPDATE Employee 
SET [Status] = 'External'
FROM Employee e
INNER JOIN Location l ON e.LocationID = l.LocationID
WHERE l.City = 'Spokane'

--3. The location for Seattle has a typo, update the street field to read 111 1st Ave

SELECT *
FROM Employee e
INNER JOIN Location l ON e.LocationID = l.LocationID
WHERE l.City = 'Seattle'

UPDATE Location
SET [Street] = '111 First Ave'
FROM Location l
WHERE l.City = 'Seattle'

--4. A new policy requires that grants for employees in Boston be made for $20,000.  There are two Boston records which aren�t set to $20,000.  Please fix them!

SELECT *
FROM Employee e
INNER JOIN Location l ON e.LocationID = l.LocationID
INNER JOIN [Grant] g ON e.EmpID = g.EmpID
WHERE l.City = 'Boston'

UPDATE g
SET g.Amount = '20000.00'
FROM Employee e
INNER JOIN Location l ON e.LocationID = l.LocationID
INNER JOIN [Grant] g ON e.EmpID = g.EmpID
WHERE l.City = 'Boston'
