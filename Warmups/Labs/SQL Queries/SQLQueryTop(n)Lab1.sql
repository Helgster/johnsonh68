--1. Find the oldest two employees in the employee table.
SELECT TOP 2 *
FROM Employee
ORDER BY HireDate

--2. Find the 6 largest grants in the grant table, include ties.
SELECT TOP 6 WITH TIES *
FROM [Grant]
ORDER BY Amount DESC

--3. Display the 10 most expensive single day trips found in the CurrentProducts table (Category = No-Stay)
SELECT TOP 10 WITH TIES *
FROM CurrentProducts
WHERE [Category] = No-Stay
ORDER BY RetailPrice DESC

