Select *
from EmployeeTerritories
INNER JOIN Employees on employees.employeeID = EmployeeTerritories.EmployeeID

Select CompanyName, OrderDate, ProductName 
FROM Orders
INNER JOIN customers on orders.customerID = customers.customerID
INNER JOIN [Order Details] on orders.orderID = [Order Details].OrderID
INNER JOIN Products on products.productID = [Order Details].ProductID
Where country = 'USA'

SELECT [Order Details].*
FROM Orders
INNER JOIN [Order Details] ON Orders.OrderID = [Order Details].OrderID
INNER JOIN Products ON Products.ProductID = [Order Details].ProductID
Where ProductName = 'Chai'