--1. Find the max unit price for each product by category

SELECT CategoryID, MAX(UnitPrice) AS MaxUnitPrice
FROM Products
GROUP BY CategoryID

--2. We want to get some customer data, create lists of customers with the following attributes:
--           1. Most orders submitted
--           2. Highest lifetime order total amount 
--              (hint, the Order table doesn�t have the total, you will need to join to order details and calculate it

SELECT c.CompanyName, COUNT(OrderID) AS MostOrdersSubmitted
FROM Customers c
INNER JOIN Orders o ON c.CustomerID = o.CustomerID
GROUP BY c.CompanyName
ORDER BY MostOrdersSubmitted

SELECT CustomerID, SUM(UnitPrice * Quantity) AS LifeTimeTotalAmount
FROM Orders o
INNER JOIN [Order Details] od ON o.OrderID = od.OrderID
GROUP BY o.CustomerID
ORDER BY LifeTimeTotalAmount DESC
