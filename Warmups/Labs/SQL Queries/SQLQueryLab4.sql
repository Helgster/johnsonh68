SELECT OrderDate
FROM Customers
INNER JOIN Orders ON Customers.CustomerID = Orders.CustomerID
WHERE Country = 'Brazil'

SELECT FirstName, Territories.*
FROM Employees
INNER JOIN EmployeeTerritories ON Employees.EmployeeID = EmployeeTerritories.EmployeeID
INNER JOIN Territories ON Territories.TerritoryID = EmployeeTerritories.TerritoryID
WHERE Employees.City = 'Seattle'

SELECT COUNT(DISTINCT RegionDescription) AS NoOfDistinctRegions
FROM Territories
INNER JOIN Region ON Territories.RegionID = Region.RegionID

SELECT OrderID, '$' + CONVERT(varchar(15), SUM([Order Details].UnitPrice * [Order Details].Quantity), 1) AS GrandTotalOrderID
FROM [Order Details]
GROUP BY OrderID

SELECT Customers.CustomerID, CompanyName, '$' + CONVERT(varchar(15), SUM([Order Details].Quantity * [Order Details].UnitPrice), 1) AS CustomerOrdersGrandTotal
FROM Customers
LEFT JOIN Orders ON Customers.CustomerID = Orders.CustomerID
LEFT JOIN [Order Details] ON Orders.OrderID = [Order Details].OrderID
GROUP BY CompanyName, Customers.CustomerID

SELECT '$' + CONVERT(varchar(20), SUM([Order Details].Quantity * [Order Details].UnitPrice), 1) AS RevenueGrandTotal
FROM [Order Details]