﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Data
{
    class OrderEntry
    {
        public string OrderNumber { get; set; }
        public string CustomerName { get; set; }
        public string State { get; set; }
        public string TaxRate { get; set; }
        public string ProductType { get; set; }
        public string Area { get; set; }
        public string CostPerSqFt { get; set; }
        public string LaborPerSqFt { get; set; }
        public string MaterialCost { get; set; }
        public string TotalLaborCost { get; set; }
        public string Tax { get; set; }
        public string OrderTotal { get; set; }
    }
}
