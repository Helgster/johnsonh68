﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SwcLms.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SwcLms.Controllers
{
    public class HomeController : Controller
    {
        GradeLevelManager _glManager = new GradeLevelManager();
        RoleManager _roleManager = new RoleManager();

        public ActionResult Index()
        {
            ViewBag.GradeLevels = _glManager.GetAllGradeLevels();
            ViewBag.Roles = _roleManager.GetAllUserRoles();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult UserDetails()
        {
            var userStore = new UserManager<IdentityUser>(new UserStore<IdentityUser>());
            var user = userStore.FindById(User.Identity.GetUserId());

            return View(user);
        }
    }
}