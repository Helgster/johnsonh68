﻿using SwcLms.Models;
using SwcLms.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwcLms.Managers
{
    public class GradeLevelManager
    {
        IGradeLevelRepository _repo = new GradeLevelRepository();

        public List<GradeLevel> GetAllGradeLevels()
        {
            return _repo.GetGradeLevels();
        }
    }
}