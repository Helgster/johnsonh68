﻿using SwcLms.Models;
using SwcLms.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwcLms.Managers
{
    public class RoleManager
    {
        IUserRepository _repo = new DbUserRepository();

        public List<RoleViewModel> GetAllUserRoles()
        {
            return _repo.GetAllRoles().Select(x =>
                new RoleViewModel()
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();
        }
    }
}