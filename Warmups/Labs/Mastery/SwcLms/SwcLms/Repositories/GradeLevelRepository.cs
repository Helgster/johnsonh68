﻿using SwcLms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwcLms.Repositories
{
    public class GradeLevelRepository : IGradeLevelRepository
    {

        public List<GradeLevel> GetGradeLevels()
        {
            using (var context = new SWC_LMSEntities())
            {
                return context.GradeLevels.ToList();
            }
        }
    }
}