﻿using SwcLms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwcLms.Repositories
{
    public interface IUserRepository
    {
        void CreateLmsUser(LmsUser user);
        List<LmsUser> GetUnauthorizedUsers();
        List<AspNetRole> GetAllRoles();
    }
}
