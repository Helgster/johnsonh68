﻿using SwcLms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwcLms.Repositories
{
    interface IGradeLevelRepository
    {
        List<GradeLevel> GetGradeLevels();
    }
}
