﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SwcLms.Startup))]
namespace SwcLms
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
