﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyContacts.UI.Models
{
    public enum NumberType
    {
        Work = 1,
        Home = 2,
        Cell = 3
    }

    public class PhoneNumber
    {
        public int PhoneNumberId { get; set; }
        public string Number { get; set; }
        public NumberType TypeOfNumber { get; set; }
    }
}
