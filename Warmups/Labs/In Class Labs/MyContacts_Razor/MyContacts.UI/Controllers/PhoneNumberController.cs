﻿using MyContacts.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyContacts.UI.Controllers
{
    public class PhoneNumberController : Controller
    {
        // GET: PhoneNumber
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult AddPhone(int contactId)
        {
            FakeContactDatabase fakeDb = new FakeContactDatabase();

            Contact theContact = fakeDb.GetById(contactId);

            ViewBag.ContactId = theContact.ContactId;
            ViewBag.ContactName = theContact.Name;

            return View();
        }

        public ActionResult AddPhoneNumber(int contactId, PhoneNumber phoneNumberToAdd)
        {
            FakeContactDatabase fakeDb = new FakeContactDatabase();

            Contact theContact = fakeDb.GetById(contactId);

            theContact.PhoneNumbers.Add(phoneNumberToAdd);

            return RedirectToAction("Edit", "Home", new { id = contactId });
        }
    }
}