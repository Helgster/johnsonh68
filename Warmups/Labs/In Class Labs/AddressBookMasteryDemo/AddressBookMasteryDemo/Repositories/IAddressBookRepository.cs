﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AddressBookMasteryDemo.Models;

namespace AddressBookMasteryDemo.Repositories
{
    public interface IAddressBookRepository
    {
        AddressBook Load();
        void Save(AddressBook addyBook);
    }
}
