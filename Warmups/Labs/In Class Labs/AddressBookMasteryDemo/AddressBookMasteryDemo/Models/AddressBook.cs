﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddressBookMasteryDemo.Models
{
    public class AddressBook
    {
        public AddressBook()
        {
            Entries = new List<Entry>();
        }

        public string Name { get; set; }
        public List<Entry> Entries { get; set; }

        public List<Entry> SearchForEntryByLastName(string searchTerm)
        {
            return this.Entries.Where(x => x.LastName.ToUpper().Contains(searchTerm.ToUpper())).ToList();
        }

        public int RemoveEntryByLastName(string lastName)
        {
            int deletedEntries = this.Entries.Where(x => x.LastName.ToUpper() == lastName.ToUpper()).Count();
            // both lines do the same thing!!!!
            //this.Entries.RemoveAll(x => x.LastName.ToUpper() != lastName.ToUpper());

            // as in this and that other line! ^^
            this.Entries = this.Entries.Where(x => x.LastName.ToUpper() != lastName.ToUpper()).ToList();

            return deletedEntries;
        }

        public Entry FindEntryToEdit(string lastName)
        {
            return this.Entries.First(x => x.LastName.ToUpper() == lastName.ToUpper());
        }

        public void SortEntries()
        {
            this.Entries = this.Entries.OrderBy(x => x.LastName).ToList();
        }
    }
}
