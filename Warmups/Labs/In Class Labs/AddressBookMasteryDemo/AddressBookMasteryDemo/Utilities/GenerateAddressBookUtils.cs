﻿using AddressBookMasteryDemo.Models;
using AddressBookMasteryDemo.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddressBookMasteryDemo.Utilities
{
    public static class GenerateAddressBookUtils
    {
        private static IAddressBookRepository _repo = new FileAddressBookRepository();

        /// <summary>
        /// This generates a demo address book
        /// </summary>
        /// <returns>The generated address book</returns>
        public static AddressBook GenerateAddressBook()
        {
            AddressBook addyBook = new AddressBook();
            foreach (int num in Enumerable.Range(1, 50))
            {
                addyBook.Entries.Add(new Entry()
                {
                    FirstName = string.Format("FNAME{0:D2}", num),
                    LastName = string.Format("LNAME{0:D2}", num),
                    PhoneNumber = string.Format("180088855{0:D2}", num),
                    Email = string.Format("EMAIL{0:D2}", num),
                    Address = string.Format("{0:D2} ADDRESS ST", num)
                });
            }
            return addyBook;
        }

        public static void GenerateAddressBookFile()
        {
            _repo.Save(GenerateAddressBook());
        }
    }
}
