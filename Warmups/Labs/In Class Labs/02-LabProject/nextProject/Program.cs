﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nextProject
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter your name:");
            string myName = Console.ReadLine();
            Console.WriteLine("Please enter your age:");
            string myAge = Console.ReadLine();
            int num = int.Parse(myAge);
            Console.WriteLine("Hello " + myName + ", you are " + num + " years old!");
            Console.ReadLine();
        }
    }
}
