﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Address
    {
        public enum StateList
        {
            MN,
            WI,
            IL,
            IA,
            SD,
            ND
        }
        //private string _firstName;
        //private string _lastName;
        //private string _address;
        //private string _city;
        //private string _state;
        private string _zipCode;
        private PhoneNumber[] _phoneNumbers = new PhoneNumber[1024];
        private string _emailAddress;
        private DateTime _birthday;

        public void AddPhoneNumber(string number, string type)
        {
            for (int i = 0; i < _phoneNumbers; i++)
            {
                if (_phoneNumbers[i] == null)
                {
                    _phoneNumbers[i] = new PhoneNumber();
                    _phoneNumbers[i].SetNumber(number);
                    _phoneNumbers[i].SetType(type);
                    break;
    
                }
                
            }
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        
    }
}
