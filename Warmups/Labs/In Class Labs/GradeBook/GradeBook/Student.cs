﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeBook
{
    public class Student
    {
        public Student()
        {
            // Let's be nice and initialize the List of Assignments so they're not null
            // when someone tries to call the Add() method on the List of Assignments object
            Assignments = new List<Assignment>();
        }

        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string SpecialNotes { get; set; }

        public List<Assignment> Assignments { get; set; }
    }
}
