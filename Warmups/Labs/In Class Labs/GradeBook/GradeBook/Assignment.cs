﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeBook
{
    public class Assignment
    {
        // We'll allow anything for the Name property so we
        // won't use a custom getter and setter
        public string Name { get; set; }


        // we're only going to allow score between 0 and 100 so we need a backing field for our
        // custom getter and setter
        private int _Score;
        /// <summary>
        /// A score between 0 and 100 (inclusive). If a value outside that range is passed, an ArguementException will be thrown.
        /// </summary>
        public int Score
        {
            get
            {
                return _Score;
            }
            set
            {
                if (value >= 0 && value <= 100)
                {
                    _Score = value;
                }
                else
                {
                    throw new ArgumentException("Score must be between 0 and 100 (inclusive)");
                }
            }
        }
    }
}
