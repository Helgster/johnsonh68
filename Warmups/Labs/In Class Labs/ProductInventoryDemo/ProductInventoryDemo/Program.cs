﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductInventoryDemo
{
    static class Program
    {
        static void Main(string[] args)
        {
            Inventory inv = new Inventory();
            inv.Products.Add(new Product());
            string input = "";

            do
            {
                Console.WriteLine("What kind of product do you want to add?");
                Console.WriteLine("Press C for coat, J for blue jean, S for shirt.");
                Console.WriteLine("Press q to stop adding products");
                input = Console.ReadLine();
                Product prod = GetProductDetails(input);
                if (prod != null)
                {
                    inv.Products.Add(prod);
                }
                
            } while (input != "q");

            // this gets the number of shirts in my inventory....
            int quantityOfShirts = inv.Products.Where(x => x is Shirt).Count();
            int quantityOfBlueJeans = inv.Products.Where(x => x is BlueJean).Count();
            int quantityOfCoats = inv.Products.Where(x => x is Coat).Count();
            int quantityOfProducts = quantityOfCoats + quantityOfBlueJeans + quantityOfShirts;
            
            Console.WriteLine("Quantity of Shirts: {0}", quantityOfShirts);
            Console.WriteLine("Quantity of Blue Jeans: {0}", quantityOfBlueJeans);
            Console.WriteLine("Quantity of Coats: {0}", quantityOfCoats);
            Console.WriteLine("Quantity of all Products: {0}", quantityOfProducts);
            Console.ReadLine();

        }

        static Product GetProductDetails(string productType)
        {
            Product retProd = new Product();

            if (productType == "C")
            {
                retProd = new Coat();
                retProd = FillInProductDetails(retProd);

                Console.WriteLine("If the coat is fur enter true, if not enter false");
                var torfStr = Console.ReadLine();
                ((Coat)retProd).IsFur = bool.Parse(torfStr);

                Console.WriteLine("Enter in the length");
                var lenStr = Console.ReadLine();
                ((Coat)retProd).Length = int.Parse(lenStr);

                return retProd;
            }
            else if (productType == "J")
            { 
                // add jeans code here
                retProd = new BlueJean() ;
                retProd = FillInProductDetails(retProd);

                Console.WriteLine("Enter in waist size");
                var waistStr = Console.ReadLine();
                ((BlueJean )retProd).WaistSize = int.Parse(waistStr);

                Console.WriteLine("Enter in the length");
                var lenStr = Console.ReadLine();
                ((BlueJean)retProd).Length = int.Parse(lenStr);

                return retProd;
            }
                
            
            else if (productType == "S")
            {
                //add shirt code here;
                retProd = new Shirt();
                retProd = FillInProductDetails(retProd);

                Console.WriteLine("Enter in neck size");
                var neckStr = Console.ReadLine();
                ((Shirt)retProd).NeckSize = int.Parse(neckStr);

                Console.WriteLine("Enter in the sleeve length");
                var lenStr = Console.ReadLine();
                ((Shirt)retProd).SleeveLength = lenStr;

                return retProd;
            }
            else
            {
                return null;
            }
        }

        static Product FillInProductDetails(Product prod)
        {
            // fill in the rest of the product properties

            Console.WriteLine("Please enter the product price");
            string decimalValue = Console.ReadLine();
            prod.Price = Decimal.Parse(decimalValue);

            Console.WriteLine("Please enter the product name");
            var name = Console.ReadLine();
            prod.Name  = name;

            Console.WriteLine("Please enter the product description");
            var desc = Console.ReadLine();
            prod.Description  = desc;

            Console.WriteLine("Please enter the product color");
            var colr = Console.ReadLine();
            prod.Color = colr;

            return prod;

            

        }
    }
}
