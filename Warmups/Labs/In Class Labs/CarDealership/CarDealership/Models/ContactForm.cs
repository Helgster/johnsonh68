﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions ;

namespace CarDealership.Models
{
    public class ContactForm : IValidatableObject
    {
        public string Name { get; set; }
        public int PurchaseTimeFrameInMonths { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public decimal? Income { get; set; }

        
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (Name == null)
            {
                errors.Add(new ValidationResult("You must provide a valid name", new string[] {"Name"}));
            }

            if (PhoneNumber == null)
            {
                errors.Add(new ValidationResult("You must provide a valid phone number", new string[] {"PhoneNumber"}));
            } // Static method: This Regex validates that my phone number is a valid format i.e. 11 numbers.
            else if (!Regex.IsMatch(PhoneNumber,
                               @"^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"))
            {
                errors.Add(new ValidationResult("You need to have a valid phone number format",
                    new string[] {"PhoneNumber"}));
            }

            
            if (Email == null)
            {
                errors.Add(new ValidationResult("You must provide a valid email address", new string[] {"Email"}));
            }

            if (Email != null && !Email.Contains('@'))
            {
                errors.Add(new ValidationResult("Your Email address must contain @ symbol", new string[] {"Email"}));
            }

            if (Income == null)
            {
                errors.Add(new ValidationResult("Income field must not be empty", new string[] { "Income" }));
            }
            if (PurchaseTimeFrameInMonths > 12 && Income < 10000)
            {
                errors.Add(new ValidationResult( "We don't want your business!!"));
            }
            return errors;
        }
    }
}