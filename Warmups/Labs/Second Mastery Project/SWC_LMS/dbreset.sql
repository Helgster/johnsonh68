use SWC_LMS;
GO

-- delete tables from "inside out" (foreign keys first)
DELETE FROM RosterAssignment;
DELETE FROM Roster;
DELETE FROM Assignment;
DELETE FROM Course;
DELETE FROM [Subject];
DELETE FROM StudentGuardian;
DELETE FROM LmsUser;
DELETE FROM GradeLevel;

-- reset all the primary keys
DBCC CHECKIDENT ('RosterAssignment', RESEED, 0);
DBCC CHECKIDENT ('Roster', RESEED, 0);
DBCC CHECKIDENT ('Course', RESEED, 0);
DBCC CHECKIDENT ('Assignment', RESEED, 0);
DBCC CHECKIDENT ('Subject', RESEED, 0);
DBCC CHECKIDENT ('LmsUser', RESEED, 0);
DBCC CHECKIDENT ('GradeLevel', RESEED, 0);

-- build some sample data!
INSERT INTO GradeLevel(GradeLevelName)
VALUES ('K'), ('1st Grade'), ('2nd Grade'), ('3rd Grade'), ('4th Grade'), ('5th Grade'),
 ('6th Grade'), ('7th Grade'), ('8th Grade'), ('Freshman'), ('Sophmore'), ('Junior'), ('Senior');

-- store id's for later sample data
DECLARE @AdminId int;
DECLARE @TeacherId int;
DECLARE @ParentId int;
DECLARE @Student1Id int;
DECLARE @Student2Id int;

-- begin user records
INSERT INTO LmsUser (Id, FirstName, LastName, Email, GradeLevelId)
SELECT u.Id, 'admin', 'tester', 'admin@swcguild.com', null
FROM ASPNetUsers u
WHERE UserName = 'admin@swcguild.com';

SET @AdminId = SCOPE_IDENTITY(); -- get the created id

INSERT INTO LmsUser (Id, FirstName, LastName, Email, GradeLevelId)
SELECT u.Id, 'teacher', 'tester', 'teacher@swcguild.com', null
FROM ASPNetUsers u
WHERE UserName = 'teacher@swcguild.com';

SET @TeacherId = SCOPE_IDENTITY(); -- get the created id

INSERT INTO LmsUser (Id, FirstName, LastName, Email, GradeLevelId)
SELECT u.Id, 'parent', 'tester', 'parent@swcguild.com', null
FROM ASPNetUsers u
WHERE UserName = 'parent@swcguild.com';

SET @ParentId = SCOPE_IDENTITY(); -- get the created id

INSERT INTO LmsUser (Id, FirstName, LastName, Email, GradeLevelId)
SELECT u.Id, 'student1', 'tester', 'student1@swcguild.com', 3
FROM ASPNetUsers u
WHERE UserName = 'student1@swcguild.com';

SET @Student1Id = SCOPE_IDENTITY(); -- get the created id

INSERT INTO LmsUser (Id, FirstName, LastName, Email, GradeLevelId)
SELECT u.Id, 'student2', 'user', 'student2@swcguild.com', 3
FROM ASPNetUsers u
WHERE UserName = 'student2@swcguild.com';

SET @Student2Id = SCOPE_IDENTITY(); -- get the created id

-- assign students to the parent guardian
INSERT INTO StudentGuardian (GuardianId, StudentId)
VALUES (@ParentId, @Student1Id), (@ParentId, @Student2Id);

-- create subjects
INSERT INTO [Subject] (SubjectName)
VALUES ('English'), ('Reading'), ('Math');

-- create courses
INSERT INTO Course (UserId, SubjectId, CourseName, CourseDescription, GradeLevel, IsArchived, StartDate, EndDate)
VALUES (@TeacherId, 1, 'English for Kiddies', 'This is an english class', 3, 0, '3/1/2015', '6/1/2015'),
	   (@TeacherId, 3, 'Basic Math', 'This is an math class', 3, 0, '3/1/2015', '6/1/2015'),
	   (@TeacherId, 3, 'Basic Math', 'This is an math class', 3, 0, '3/1/2015', '6/1/2015');

-- create assignments
INSERT INTO Assignment (CourseId, AssignmentName, AssignmentDescription, PossiblePoints, DueDate)
VALUES 
-- english
(1, 'Homework 1', 'Write a sentence.', 10, '3/5/2015'),
(1, 'Homework 2', 'Write a paragraph.', 10, '3/12/2015'),
(1, 'Quiz 1', 'Show me what you have learned.', 20, '3/15/2015'),
(1, 'Test 1', 'The big test!', 100, '3/25/2015'),
-- Math
(2, 'Problem Set 1', 'Addition.', 10, '3/5/2015'),
(2, 'Problem Set 2', 'Subtraction.', 10, '3/12/2015'),
(2, 'Quiz 1', 'Show me what you have learned.', 20, '3/15/2015'),
(2, 'Test 1', 'The big test!', 100, '3/25/2015');

-- create roster
INSERT INTO Roster (CourseId, UserId, CurrentGrade, IsDeleted)
VALUES 
(1, @Student1Id, 'A+', 0),
(1, @Student2Id, 'C', 0),
(2, @Student1Id, 'B-', 0);

-- create RosterAssignments
INSERT INTO RosterAssignment (RosterId, AssignmentId, PointsEarned, Percentage, Grade)
VALUES 
-- Student 1 english
(1, 1, 10, 100.00, 'A+'), -- Homework 1
(1, 2, 10, 100.00, 'A+'), -- Homework 2
-- Student 2 english
(2, 1, 7, 70.00, 'C-'), -- Homework 1
(2, 2, 8, 80.00, 'B-'), -- Homework 2
-- Student 2 math
(2, 5, 8, 80.00, 'B-'), -- Problem Set 1
(2, 6, 8, 80.00, 'B-') -- Problem Set 2
