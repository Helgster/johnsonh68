USE SWC_LMS;
GO

	IF OBJECT_ID('GradeLevelSelectAll', 'P') IS NOT NULL
	DROP PROCEDURE GradeLevelSelectAll
GO

	CREATE PROCEDURE GradeLevelSelectAll AS

	SELECT GradeLevelId, GradeLevelName
	FROM GradeLevel

GO

	IF OBJECT_ID('LmsUserSelectByUserId', 'P') IS NOT NULL
	DROP PROCEDURE LmsUserSelectByUserId
GO

	CREATE PROCEDURE LmsUserSelectByUserId (
		@UserId int
	) AS

	SELECT UserId, Id, FirstName, LastName, Email, GradeLevelId, SuggestedRole
	FROM LmsUser
	WHERE UserId = @UserId

GO

	IF OBJECT_ID('LmsUserSelectByAspNetId', 'P') IS NOT NULL
	DROP PROCEDURE LmsUserSelectByAspNetId
GO

	CREATE PROCEDURE LmsUserSelectByAspNetId (
		@Id nvarchar(128)
	) AS

	SELECT UserId, Id, FirstName, LastName, Email, GradeLevelId, SuggestedRole
	FROM LmsUser
	WHERE Id = @Id

GO

	IF OBJECT_ID('LmsUserInsert', 'P') IS NOT NULL
	DROP PROCEDURE LmsUserInsert
GO

	CREATE PROCEDURE LmsUserInsert(
		@Id nvarchar(128),
		@FirstName varchar(30),
		@LastName varchar(30),
		@Email varchar(50),
		@GradeLevelId tinyint,
		@SuggestedRole varchar(50),
		@UserId int output -- we will assign this in the sproc and send the data back
	) AS

	INSERT INTO LmsUser(Id, FirstName, LastName, Email, GradeLevelId, SuggestedRole)
	VALUES (@Id, @FirstName, @LastName, @Email, @GradeLevelId, @SuggestedRole)

	SET @UserId = SCOPE_IDENTITY(); -- Scope_Identity() returns the most recent identity value created

GO

	IF OBJECT_ID('LmsUserSelectUnassigned', 'P') IS NOT NULL
	DROP PROCEDURE LmsUserSelectUnassigned
GO

	CREATE PROCEDURE LmsUserSelectUnassigned AS

	SELECT u.UserId, u.Id, u.FirstName, u.LastName, u.Email, u.SuggestedRole, u.GradeLevelId
	FROM LmsUser u
		INNER JOIN AspNetUsers ON u.Id = AspNetUsers.Id
		LEFT JOIN AspNetUserRoles ON AspNetUsers.Id = AspNetUserRoles.UserId
	WHERE AspNetUserRoles.UserId IS NULL

GO

	IF OBJECT_ID('LmsUserSelectRoleNames', 'P') IS NOT NULL
	DROP PROCEDURE LmsUserSelectRoleNames
GO

	CREATE PROCEDURE LmsUserSelectRoleNames
		@UserId int
	AS

	SELECT AspNetRoles.Name
	FROM LmsUser u
		INNER JOIN AspNetUsers ON u.Id = AspNetUsers.Id
		INNER JOIN AspNetUserRoles ON AspNetUsers.Id = AspNetUserRoles.UserId
		INNER JOIN AspNetRoles ON AspNetUserRoles.RoleId = AspNetRoles.Id
	WHERE u.UserId = @UserId

GO

	IF OBJECT_ID('GetStudentsInCourse', 'P') IS NOT NULL
	DROP PROCEDURE GetStudentsInCourse
GO

CREATE PROCEDURE GetStudentsInCourse(
	@CourseId int
) AS
	SELECT c.CourseName, u.UserId as StudentId, u.FirstName, u.LastName, u.Email
	FROM Course as c
		inner join Roster as r on c.CourseId = r.CourseId
		inner join LmsUser as u on r.UserId = u.UserId
	WHERE c.CourseId = @CourseId and r.IsDeleted = 0
go

	IF OBJECT_ID('GetStudentAndRosterId', 'P') IS NOT NULL
	DROP PROCEDURE GetStudentAndRosterId
GO
CREATE PROCEDURE GetStudentAndRosterId(
	@CourseId int
) AS
	SELECT u.FirstName, u.LastName, r.RosterId, r.CurrentGrade
	FROM Course as c
		inner join Roster AS r ON c.CourseId = r.CourseId
		inner join LmsUser AS u ON r.UserId = u.UserId
	WHERE c.CourseId = @CourseId and r.IsDeleted = 0
	ORDER BY u.LastName
GO

	IF OBJECT_ID('GetRosterAssignments', 'P') IS NOT NULL
	DROP PROCEDURE GetRosterAssignments
GO
CREATE PROCEDURE GetRosterAssignments(
	@RosterId int
) AS
	SELECT ra.RosterAssignmentId, ra.AssignmentId, ra.PointsEarned, ra.Percentage, a.PossiblePoints
	FROM RosterAssignment as ra
		inner join Roster as r on ra.RosterId = r.RosterId
		right join Assignment as a on ra.AssignmentId = a.AssignmentId
	WHERE ra.RosterId = @RosterId and r.IsDeleted = 0
	ORDER BY a.DueDate, a.AssignmentId
GO

	IF OBJECT_ID('GetAssignmentsInCourse', 'P') IS NOT NULL
	DROP PROCEDURE GetAssignmentsInCourse
GO
CREATE PROCEDURE GetAssignmentsInCourse(
	@CourseId int
) AS 
	SELECT c.CourseName, a.AssignmentId, a.AssignmentName, a.AssignmentDescription, a.PossiblePoints, a.DueDate
	FROM Course as c
		INNER JOIN Assignment as a on c.CourseId = a.CourseId
	WHERE c.CourseId = @CourseId
go

	IF OBJECT_ID('GetAssignmentGrades', 'P') IS NOT NULL
	DROP PROCEDURE GetAssignmentGrades
GO

CREATE PROCEDURE GetAssignmentGrades(
	@RosterId int
) AS 
SELECT r.Grade, r.Percentage, a.AssignmentName
FROM RosterAssignment r
	RIGHT JOIN Assignment a on r.AssignmentId = a.AssignmentId	
WHERE r.RosterId = @RosterId
GO

	IF OBJECT_ID('UpdateRosterCurrentGrade', 'P') IS NOT NULL
	DROP PROCEDURE UpdateRosterCurrentGrade
GO
CREATE PROCEDURE UpdateRosterCurrentGrade(
	@RosterAssignmentId int
) AS

    -- get roster id for the roster assignment
	DECLARE @RosterId int;

	SELECT @RosterId = RosterId 
	FROM RosterAssignment 
	WHERE RosterAssignmentId = @RosterAssignmentId;

	-- calculate total points in class
	DECLARE @PointsEarnedTotal decimal(8,2);
	DECLARE @PointsPossibleTotal decimal(8,2);

	SELECT @PointsEarnedTotal = sum(PointsEarned), @PointsPossibleTotal = sum(PossiblePoints)  
	FROM RosterAssignment ra 
		inner join Assignment a on ra.AssignmentId = a.AssignmentId 
	WHERE ra.RosterId = @RosterId and PointsEarned is not null;

	-- Update the grade in the class
	declare @ClassPercentage decimal(8,2);
	SET @ClassPercentage = ROUND(@PointsEarnedTotal / @PointsPossibleTotal,2)
	UPDATE Roster
	SET CurrentGrade = CASE WHEN @ClassPercentage < 60 THEN 'F' 
							WHEN @ClassPercentage < 70 THEN 'D'
							WHEN @ClassPercentage < 80 THEN 'C'
							WHEN @ClassPercentage < 90 THEN 'B'
							ELSE 'A' END
	WHERE RosterId = @RosterId
go

	IF OBJECT_ID('UpdateAssignmentGrade', 'P') IS NOT NULL
	DROP PROCEDURE UpdateAssignmentGrade
GO
CREATE PROCEDURE UpdateAssignmentGrade(
	@RosterAssignmentId int,
	@PointsEarned decimal(7,2),
	@Percentage decimal(5,2),
	@GradeLetter int
) as
	UPDATE RosterAssignment SET
		PointsEarned = @PointsEarned,
		Percentage = @Percentage,
		Grade = @GradeLetter
	WHERE RosterAssignment.RosterAssignmentId = @RosterAssignmentId;

	-- also update their overall class grade
	EXEC UpdateRosterCurrentGrade @RosterAssignmentId;
go