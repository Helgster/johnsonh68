﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SwapMethodIQ
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter a number: ");
             var a = Int32.Parse(Console.ReadLine());
            Console.Write("Enter another number: ");
            var b = Int32.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("The value of the first number is: {0}", a);
            Console.WriteLine("The value of the second number is: {0}", b);
            Console.Write("Press enter and I will reverse the numbers for you: ");
            Console.ReadLine();
            Swap(a, b);
            Console.WriteLine("The first number you entered is now: {0}", a);
            Console.WriteLine("The second number you entered is now: {0}", b);
            Console.ReadLine();
        }
        public static void Swap(int a, int b)
        {
            a = a + b; //a = 21, b = 8
            b = a - b; //a = 21, b = 13
            a = a - b; //a = 8, b = 13 
        }
    }
}
