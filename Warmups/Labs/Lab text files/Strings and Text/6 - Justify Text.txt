Justify Text
Difficulty: 6

Given an array of words and and a line length, format the text such that each line has no more than L characters and is fully (left and right) justified.

- Extra spaces between words should be distributed as evenly as possible.
- If the number of spaces on a line do not divide evenly between words, the empty slots on the left will be assigned more spaces than the right.
- The last line should be left justified and no extra spaces between words.
- A line other than the last might only fit one word, in this case, left justify it.

For example: ["This", "is", "an", "example", "of", "text", "justification"], 16
Returns: [
	"This    is    an",
	"example  of text",
	"justification.  "
]


Tips
-------------------------------
Figure out which words go on which lines, then worry about the spacing.