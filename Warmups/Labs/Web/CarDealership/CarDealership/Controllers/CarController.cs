﻿using CarDealership.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.Mvc;
using CarDealership.Models;

namespace CarDealership.Controllers
{
    public class CarController : Controller
    {
        ICarRepository _repo = new DBCarRepository();

        // GET: Car
        public ActionResult Index()
        {
            var cars = _repo.GetAllCars();
            return View(cars);
        }

        public ActionResult Details(int id)
        {
            var car = _repo.GetCarById(id);
            return View(car);
        }

        public ActionResult Add()
        {
            ViewBag.Message = "Add a car using the form below!";
            return View();
        }

        [HttpPost]
        public ActionResult AddCar(Car c)
        {
            var database = new DBCarRepository();

            // send the new car record to the database
            database.AddCar(c);

            // tell the browser to navigate to Car/Index
            return RedirectToAction("Index", "Car");
        }

        public ActionResult Edit(int id)
        {
            var database = new DBCarRepository();
            var carDetails = database.GetCarById(id);
            ViewBag.Message = "Edit this car in the fields below!";
            return View(carDetails);
        }

        [HttpPost]
        public ActionResult EditCar(Car c)
        {
            var database = new DBCarRepository();

            //send editted car details to car database
            database.EditCar(c);

            //tell the browser to return to the Car/Index page
            return RedirectToAction("Index", "Car");
        }

        public ActionResult Delete(int id)
        {
            var car = _repo.GetCarById(id);
            ViewBag.Message = "Are you sure you want to delete this car from our website?";
            return View(car);
        }

        [HttpPost]
        public ActionResult DeleteCar(int id)
        {
            //remove car details from car database
            _repo.DeleteCar(id);

            //tell the browser to return to the Car/Index page
            return RedirectToAction("Index", "Car");

           }
    }
}