﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_ShapesAreasPerimeters
{
    //The base class “Shape” will have the two methods area() and perimeter() but they will be empty.

    public abstract class Shape
    {
        public string name;

        public Shape(string s)
        {
            Id = s;
        }

        public string Id
        {
            get
            {
                return name;
                
            }
            set
            {
                name = value;
                
            }
        }

        public abstract double Area
        {
            get;
            
        }

       public abstract double Perimeter
        {
            get;
            
        }
        
        public override string ToString()
        {
            return String.Format("ID: {0, -12} Perimeter: {1, 6:F2}   Area: {2, 6:F2}", Id, Perimeter, Area);
        }
    }
}
