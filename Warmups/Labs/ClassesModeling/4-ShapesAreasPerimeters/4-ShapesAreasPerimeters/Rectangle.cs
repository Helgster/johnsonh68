﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_ShapesAreasPerimeters
{
    public class Rectangle : Shape 
    {
        private int width;
        private int height;

    public Rectangle(int width, int height, string Id)
        : base(Id)
    {
        this.width = width;
        this.height = height;
    }

    public override double Area
    {
        get
        {
            // Given the width and height, return the area of a rectangle: 
            return width * height;
        }
    }

        public override double Perimeter
    {
         get
         {
                return 2*(width + height);
                
         }
    }

   }

}
