﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace _4_ShapesAreasPerimeters
{
    public class Triangle : Shape
    {
        private int width;
        private int height;
        private int sideb;
        
        public Triangle(int width, int height, int sideb, string id) : base(id)
        {
            this.width = width;
            this.height = height;
            this.sideb = sideb;
        }

        public override double Area
        {
            get
            {
                // Given the width and height, return the area of a triangle:
                return 0.5 * width * height;
            }
        }
        
        
        public override double Perimeter
        {
            get
            {
                //Given the width, height, and sideb, return the perimeter of a triangle:
                return width + height + sideb;
            }
        }
    }
}
