﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_ShapesAreasPerimeters
{
    internal class Program
    {
        //Shape Area and Perimeter Classes
        //Difficulty: 3

        //Create a set of classes that represent a rectangle, triangle and circle. 
        //Have these classes inherit from a base class called “Shape” and each of them will implement at least two methods. 
        //One called area(), which will return the area of the shape, and another called “perimeter()” which will return the perimeter of the shape.

        //Tips
        //-------------------------------
        //The base class “Shape” will have the two methods area() and perimeter() but they will be empty. 
        //They are designed to be overridden by inherited shapes. 
        //So make sure that any shape that you inherit from the base class implements their own versions of area() and perimeter() based on the type of shape it is. 
        //It is suggested you start with a square since this should be the easiest to implement. 
        //Create a Shape base class, inherit a square from it and override the two methods. 
        //This should give you the idea for the others if you have done things correctly.

        //Added Difficulty
        //-------------------------------
        //Can you make the shapes three dimensional objects like a cube, sphere and pyramid and return the area() for them?

        public static void Main(string[] args)
        {
            Shape[] shapes =
            {
                new Rectangle(5, 12, "Rectangle #1"),
                new Circle(4, "Circle # 1"),
                new Triangle(5, 4, 8, "Triangle #1")
            };
            Console.WriteLine("Shapes Collection");
            foreach (Shape s in shapes)
            {
                Console.WriteLine(s);
            }
            Console.ReadLine();
        }
    }
}
