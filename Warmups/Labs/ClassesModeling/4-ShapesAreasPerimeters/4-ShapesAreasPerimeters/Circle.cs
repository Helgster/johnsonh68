﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_ShapesAreasPerimeters
{
    public class Circle : Shape
    {

        private int radius;

        public Circle(int radius, string id) : base(id)
        {
            this.radius = radius;
        }

        public override double Area
        { 
            get
            {
                //Given the radius, return the area of a circle:
                return radius * radius * System.Math.PI;
            }
        }

        public override double Perimeter
        {
            get
            {
                return 2 * System.Math.PI * radius;
            }
        }
    }
}
