﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Cars
{
    class Cars
    {
        public string Color { get; set; }
        private string Make = "Mazda";
        private string Model = "Miata";
        private int Year = 2000;
        private int NumberOfDoors = 2;

        public void MyCars()
        {
            Console.WriteLine("{0} : {1} : {2} : {3} : {4} Door", Year, Make, Model, Color, NumberOfDoors);
        }
    }
   
}
