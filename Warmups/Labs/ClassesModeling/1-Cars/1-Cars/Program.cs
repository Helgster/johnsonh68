﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Cars
{
    internal class Program
    {
        //Car Project
        //Difficulty: 1

        //Create a console program and a class that models a car.
        //In our case, we want to track the state of the color, make, model, year, and number of doors the car currently has.
        //In addition we want to be able to paint the car a different color.

        //Intially the car should be a 2000 black Mazda Miata.

        //At the end of our program it should be a 2000 white Mazda Miata.

        //Tips
        //----------------------------
        //Remember that generally speaking, adjectives and nouns are properties and that actions and verbs are methods.

        //Added Difficulty
        //----------------------------
        //Make sure that only the color can be changed after the car has been created.
        private static void Main(string[] args)
        {
            Cars car1 = new Cars
            {
                Color = "black"
            };
            Cars car2 = new Cars
            {
                Color = "white"
            };
            car1.MyCars();
            car2.MyCars();
            Console.ReadLine();

        }

    }
}
