﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6___GradeBook
{
    class Student
    {
        public Student()
        {
            Assignments = new List<Assignment>();
        }


        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string SpecialNotes { get; set; }
        public List<Assignment> Assignments { get; set; } 

    }
}
