﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace _2_EmployeeProject
{
    //employee class should have a first name, last name, title, the date they started at the company and the departments that he/she belongs to
    internal class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public DateTime StartDate { get; set; }
        public List<Department> Departments { get; set; }

        public Employee()
        {
            Departments = new List<Department>();
        }
        public string sayHi()
        {
            return ("Hello, " + FirstName + " " + LastName + "! How are you today?");
        }

        public string ShowDepartments()
        {
            var s1 = "I work in the ";
            foreach (var i in Departments)
                s1 = s1 + i.Name + " ";
            s1 = s1 + "Department";
            if (Departments.Count > 1)
            {
                s1 = s1 + "s";
            }
            return s1;
        }
    }
}
