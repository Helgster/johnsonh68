﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_EmployeeProject
{
    //The department class should have a name, location and it should only be able to change locations, not names.
    class Department
    {
        private string _name;
        public string Name { get { return _name; } }
        public string Location { get; set; }

        public Department(string name, string location = "default")
        {
            _name = name;
            Location = location;
        }
    }
}
