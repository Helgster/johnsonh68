﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_EmployeeProject
{
    class Program
    {
        //Employee Project
        //Difficulty: 2

        //Create a console program and a class that models employees and a class that models departments.
        //An employee can belong to 1 or more departments at our company. 
        //For example, he/she could be in the Accounting department and the Executive department.

        //The employee class should have a first name, last name, title, the date they started at the company and the departments that he/she belongs to. 
        //In addition, they should be able to say hello with their own full name.

        //The department class should have a name, location and it should only be able to change locations, not names.
        
        //Tips
        //----------------------------
        //Remember that objects can be simple, but they can also be complex

        //Added Difficulty
        //----------------------------
        //Let employees say hello to each other, not just say hello and then their own full name.
        
        static void Main(string[] args)
        {
           Employee Helge = new Employee
                {
                    FirstName = "Helge",
                    LastName = "Johnson",
                    JobTitle = "Developer",
                    StartDate = new DateTime(1989, 10, 09),
                    Departments = new List<Department> { new Department( "IT", "1st Floor"), new Department("Dad")}
                };

           Employee Karen = new Employee
                {
                    FirstName = "Karen",
                    LastName = "Johnson",
                    JobTitle = "Internal Auditor",
                    StartDate = new DateTime(2001, 5, 5),
                    Departments = new List<Department>{new Department("Auditing")}
                };

            
            Console.WriteLine(Karen.sayHi());
            Console.WriteLine(Karen.ShowDepartments());
            Console.WriteLine(Helge.sayHi());
            Console.WriteLine(Helge.ShowDepartments());
            Console.ReadLine();
        }
    }
}