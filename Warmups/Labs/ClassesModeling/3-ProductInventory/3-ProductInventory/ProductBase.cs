﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_ProductInventory
{
    public class ProductBase
    {
        public string Title { get; set; }
        public string Size { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
    }
}
