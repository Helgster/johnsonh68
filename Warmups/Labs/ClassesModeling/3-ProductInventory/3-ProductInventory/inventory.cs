﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_ProductInventory
{
    public class inventory
    {
        private List<ProductBase> items = new List<ProductBase>();

        public override string ToString()
        {
            var ret = new StringBuilder(String.Format("Inventory:{0}", Environment.NewLine));

            foreach (var p in items)
            {
                ret.AppendLine(string.Format("name: {0, -10}  description: {1, -15}  size: {2, -8}  price: {3, 6:C}", p.Title, p.Description, p.Size, p.Price));
            }

            ret.AppendFormat("item count: {0}", items.Count);
            ret.Append(Environment.NewLine);

            return ret.ToString();
        }

        public void AddItem(ProductBase p)
        {
            items.Add(p);
        }

        public void ClearInventory()
        {
            items.Clear();
        }
    }
}
