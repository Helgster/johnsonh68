﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace TwoDiceRollerLab
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //Write an application that rolls two dice 100 times. 
            //Keep track of the number of times each value (2-12 with standard dice) comes up. 
            //Print all of the values and the number of times they were rolled.

            var rng = new Random();

            int die1;
            int die2;

            List<int> rolledNums = new List<int>();
           
            // rolls the two die 100 times
            for (int i = 0; i < 100; i++)
            {
                die1 = rng.Next(1, 7);
                die2 = rng.Next(1, 7);
               
            // adds the number rolled to the rolledNums List
                rolledNums.Add(die1 + die2);
            }
            // pulls the rolled numbers out of the rolledNums list
            for (int index = 2; index <= 12; index++)
            { 
            //counts the number of times each combination was rolled
            var number = rolledNums.Count(i => i == index);
                Console.WriteLine("{0}{1} was rolled {2}{3} times.", (index <= 9 ? " " : ""), index, (number <= 9 ? " " : ""), number);
            }
            //pauses the console to read the printed items
            Console.ReadLine();

           }

        }
    }