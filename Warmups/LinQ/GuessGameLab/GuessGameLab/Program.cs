﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessGameLab
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rando = new Random();
            int numberToGuess = rando.Next(1, 21);
            string input = "";
            bool isNumberGuessed = false;
            int playerGuess;
            List<int> guesses = new List<int>();

            do
            {
                Console.WriteLine("Please guess a number");
                input = Console.ReadLine();

                // attempt to convert to number
                if (int.TryParse(input, out playerGuess))
                {
                    if (guesses.Contains(playerGuess))
                    {
                        // Tell the user nicely they're an idiot.
                        Console.Write("You already guessed that. ");
                    }
                    else
                    {
                        guesses.Add(playerGuess);
                    }

                    // see if they won
                    if (playerGuess == numberToGuess)
                    {
                        Console.WriteLine("You got it!");
                        isNumberGuessed = true;
                    }
                    else
                    {
                        // they didn't win, so were they too high or too low?
                        if (playerGuess > numberToGuess)
                            Console.WriteLine("Too high!");
                        else
                            Console.WriteLine("Too low!");
                    }
                }
                else
                {
                    // learn to play n00b
                    Console.WriteLine("That wasn't a valid number!");
                }
            } while (!isNumberGuessed);

            Console.WriteLine("Your guesses were: ");

            // this does the same as....
            guesses.ForEach(x => Console.WriteLine(x));

            // this and the same as
            //foreach (int guess in guesses)
            //{
            //    Console.WriteLine(guess);
            //}

            // this
            //for (int i = 0; i < guesses.Count; i++)
            //{
            //    Console.WriteLine(guesses[i]);
            //}

            Console.ReadLine();
        }

        public static bool ValidatedNumber(string strNumber)
        {
            int inputtedNumber;
            bool isANumber = int.TryParse(strNumber, out inputtedNumber);
            if (isANumber)
            {
                if (inputtedNumber < 1)
                {
                    return false;
                }
                else if (inputtedNumber > 20)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
