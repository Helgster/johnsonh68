﻿using System;
using System.Linq;

namespace LINQ
{
    internal class Program
    {
        /* Practice your LINQ!
         * You can use the methods in Data Loader to load products, customers, and some sample numbers
         * 
         * NumbersA, NumbersB, and NumbersC contain some ints
         * 
         * The product data is flat, with just product information
         * 
         * The customer data is hierarchical as customers have zero to many orders
         */

        private static void Main()
        {
            //PrintOutOfStock();
            //PrintInStockGreaterThan3();
            //WaCust();
            //PrintProductsNames();
            //PrintProductsNamesIncUnitPr25();
            //PrintProductsNamesUpperCase();
            //PrintProductsWEvenNoInStk();
            //ProductCatPrice();

            Console.ReadLine();
        }

        //1. Finds all products that are out of stock.
        private static void PrintOutOfStock()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                              where p.UnitsInStock == 0
                              select p;

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }

        }

        //2. Finds all products that are in stock and cost more than 3.00 per unit.
        private static void PrintInStockGreaterThan3()
        {
            var products = DataLoader.LoadProducts();

            var col = from p in products
                where p.UnitsInStock > 0
                where p.UnitPrice >= 3
                select p;


            foreach (var product in col)
            {
                Console.WriteLine("{0, -33}{1, 8:C}", product.ProductName, product.UnitPrice);
            }
        }

        //3. Find all customers in Washington, print their name then their orders. (Region == "WA")
        private static void WaCust()
        {
            var customers = DataLoader.LoadCustomers();

            var co = from o in customers
                where o.Region == "WA"
                select o;


            foreach (var customer in co)
            {
                Console.WriteLine(customer.CompanyName);

               foreach (var order  in customer.Orders)
               {
                   var order1 = order.OrderID;
                   var order2 = order.OrderDate;
                   var order3 = order.Total;
                   Console.WriteLine("\t{0, -10} {1, 12:d} {2, 12:C}", order1, order2, order3);
               }
            }

        }

        //4. Creates a new sequence with just the names of the products.
        private static void PrintProductsNames()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Select(x => x.ProductName);

            foreach (var productName in results)
            {
                Console.WriteLine(productName);
            }
        }

        //5. Create a new sequence of products and unit prices where the unit prices are increased by 25%.
        private static void PrintProductsNamesIncUnitPr25()
        {
            var products = DataLoader.LoadProducts();

            var p1 = from p in products
                where p.UnitPrice >= 0
                select p;

            foreach (var i in p1)
            {
                Console.WriteLine("{0, -33}{1, 10:C}", i.ProductName, i.UnitPrice*1.25m);
            }
        }

        //6. Creates a new sequence of just product names in all upper case.
        private static void PrintProductsNamesUpperCase()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                select p.ProductName.ToUpper();
            
            foreach (var i in results)
            {
                Console.WriteLine("{0}", i);
            }

        }

        //7. Create a new sequence with products with even numbers of units in stock.
        private static void PrintProductsWEvenNoInStk()
        {
            var products = DataLoader.LoadProducts();

            var evenInStk = from p in products
                where p.UnitsInStock % 2 == 0
                select p;
            
            foreach (var i in evenInStk)
            {
                Console.WriteLine("{0}", i.ProductName);
            }
        }

        //8. Create a new sequence of products with ProductName, Category, and rename UnitPrice to Price.
        private static void ProductCatPrice()
        {
            var products = DataLoader.LoadProducts();

            var prods = products.Select(x => new
            {
                ProductName = x.ProductName, 
                Category = x.Category,
                Price = x.UnitPrice
            });

            foreach (var i in prods)
            {
                Console.WriteLine("{0, -35}{1, -15}{2, 8:C}", i.ProductName, i.Category, i.Price);
            }
        }
    }
}

