﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayPractice1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            int[] n = new int[6]; /* n is an array of 10 integers */
            int i, j;

            /* initialize elements of array n */
            for (i = 0; i < 6; i++)
            {
                n[i] = i * 2;
            }

            /* output each array element's value */
            for (j = 0; j < 6; j++)
            {
                Console.WriteLine("Element[{0}] = {1}", j, n[j]);
            }

        //int[] nums = new int[4];
            //int i, j;

            ///* initialize elements of array n */
            //for (i = 0; i < 4; i++)
            //{
            //    nums[i] = i + 100;
            //}

            ///* output each array element's value */
            //for (j = 0; j < 10; j++)
            //{
            //    Console.WriteLine("Element[{0}] = {1}", j, nums[j]);
            //}

            Console.ReadLine();

            //nums[0] = 0;
            //nums[1] = 1;
            //nums[2] = 2;
            //nums[3] = 3;

            //for (int i = 0; i < 4; i++)
            //Console.WriteLine(i);

            //Console.ReadLine();
        }


    }
}
    