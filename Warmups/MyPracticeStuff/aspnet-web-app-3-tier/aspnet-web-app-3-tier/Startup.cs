﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(aspnet_web_app_3_tier.Startup))]
namespace aspnet_web_app_3_tier
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
