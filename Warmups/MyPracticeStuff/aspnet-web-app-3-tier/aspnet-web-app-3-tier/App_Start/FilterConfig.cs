﻿using System.Web;
using System.Web.Mvc;

namespace aspnet_web_app_3_tier
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
