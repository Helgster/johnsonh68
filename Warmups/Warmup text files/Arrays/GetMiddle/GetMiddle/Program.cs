﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GetMiddle
{
    class Program
    {
        static void Main(string[] args)
        {
            // I can give it a 3
            int[] arr1 = new int[3] { 1, 2, 3 };
            // or I can have it assume the array length is 3
            int[] arr2 = new int[] { 4, 5, 6 };

            Console.Write(arr1[1] + arr2[1] + "\n");

            var result = GetMiddle(arr1, arr2);

            // prints out each number in the returned array
            foreach (int number in result)
            {
                //Console.WriteLine(number);
            }
            // just to pause the code
            Console.ReadLine();
        }

        /// <summary>
        /// Gets the middle of each array
        /// </summary>
        /// <param name="a">An array with a length of 3</param>
        /// <param name="b">An array with a length of 3</param>
        /// <returns>An array of length 2 that contains the middle of a and b</returns>
        public static int[] GetMiddle(int[] a, int[] b)
        {
            int[] returnedArray = new int[2];

            for (int i = 0; i < a.Length; i++)
            {
                if (i == 1)
                {
                    returnedArray[0] = a[i];
                    returnedArray[1] = b[i];
                }
            }

            return returnedArray;
        }

        //Given 2 int arrays, a and b, each length 3, return a new array length 2 containing their middle elements. 

        //GetMiddle({1, 2, 3}, {4, 5, 6}) -> {2, 5}
        //GetMiddle({7, 7, 7}, {3, 8, 0}) -> {7, 8}
        //GetMiddle({5, 2, 9}, {1, 4, 5}) -> {2, 4}

        //public int[] GetMiddle(int[] a, int[] b) {

        //}
    }
}
